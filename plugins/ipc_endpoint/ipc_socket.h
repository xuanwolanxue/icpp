/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: ipc_socket.h
 *
 * Purpose: wrapper unix domain socket 
 *
 * Developer:
 *   wen.gu , 2021-10-12
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_COM_IPC_SOCKET_H__
#define __ICPP_COM_IPC_SOCKET_H__

#include "icpp/com/endpoint.h"

/******************************************************************************
 **    MACROS
 ******************************************************************************/

#define INVALID_SOCKET_CLIENT_ID (-1)
/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{

/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/
class IpcSocketService final
{
public:
    
    using OnReceiveHandler = std::function<void(int client_id, const uint8_t* buf, uint32_t len)>;
    using OnAddClientHandler = std::function<void(int client_id, const std::string& url_addr)>;
    using OnDelClientHandler = std::function<void(int client_id)>;
public:
    IpcSocketService(const std::string& addr);
    ~IpcSocketService();
public:
    core::IcppErrc start(OnReceiveHandler on_receive, OnAddClientHandler on_add_client, OnDelClientHandler on_del_client);
    core::IcppErrc stop();

    /** if client_id <= -1, then do broadcast for all client */
    core::IcppErrc send(int client_id, const uint8_t* buf, uint32_t len);   

private:
    class Impl;
    std::unique_ptr<Impl> impl_;
};


class IpcSocketClient final
{
public:
    using OnConnectHandler = std::function<void(int socket_id, bool is_connected)>;
    using OnReceiveHandler = std::function<void(const uint8_t* buf, uint32_t len)>;
public:
    /**
     * if bool is_auto_reconect is true: then the client will try to reconnect to service when the connection is broken.
     */
    IpcSocketClient(const std::string& addr, bool is_auto_reconect);
    ~IpcSocketClient();
public:

    core::IcppErrc start(OnConnectHandler on_connect, OnReceiveHandler on_receive);
    core::IcppErrc stop();
    core::IcppErrc send(const uint8_t* buf, uint32_t len);   

private:
    class Impl;
    std::unique_ptr<Impl> impl_;
};



} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_COM_IPC_SOCKET_H__ */

