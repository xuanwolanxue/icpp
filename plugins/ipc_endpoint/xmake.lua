target("ipc_endpoint")
    set_kind("shared")	
	if is_plat("windows") then
		add_defines("COM_DLL_EXPORT")
		add_links("wsock32")
	end
    add_files("./*.cpp")
	add_deps("icpp_core", "icpp_executor", "icpp_com")
	add_links("icpp_core", "icpp_executor", "icpp_com")