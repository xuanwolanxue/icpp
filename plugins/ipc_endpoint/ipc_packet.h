/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: ipc_packet.h
 *
 * Purpose: data packet parser and builder 
 *
 * Developer:
 *   wen.gu , 2021-10-15
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_COM_IPC_PACKET_H__
#define __ICPP_COM_IPC_PACKET_H__

#include "icpp/com/types.h"
#include "icpp/com/message.h"


/******************************************************************************
 **    MACROS
 ******************************************************************************/

#define IPC_PKT_START_CODE ((uint8_t)0xe5)
#define IPC_PKT_END_CODE ((uint8_t)0xe6)

/** transport packet(protocol) 
 *  |start code|service id|message id|length|client id|session id|protocol version|interface version|type|error code|payload|end code|
 *  start code: 1 bytes
 *  service id: 2 bytes
 *  message id: 2 bytes
 *  length:     4 bytes
 *  client id:  2 bytes
 *  session id: 2 bytes
 *  protocol version: 1 byte
 *  interface version: 1 byte
 *  type:       1 byte
 *  error code: 1 byte
 *  payload:       n bytes
 *  end code:   1 bytes
 */

/** the size of packet header, |start code|service id|message id|length|client id|session id|protocol version|interface version|type|error code| */
#define IPC_PKT_HDR_SIZE  16 

/** the length segment include: |client id|session id|protocol version|interface version|type|error code|payload size| 
 *  so it's  minimum value must include:  |client id|session id|protocol version|interface version|type|error code|
 */
#define IPC_LENGTH_MIN  8

/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{

enum class IpcDataParseState: uint8_t
{
    Start = 0,  /** check start code, then switch to 'Header' */
    Header,     /** receive and parse Header, then switch to 'Payload' */
    Payload,    /** receive payload data, then switch to 'End' */
    End,        /** check end code, if success then do callback. both success and fail then siwtch to 'Start' */
};

/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/

/** from packet raw data parser to a communication message */ 
class IpcPacketParser final
{
public:
    /** params:
     *  [1] MessagePtr: received data as a message.
     */
    using DataReceiveHandler = std::function<void(Message::MessagePtr)>;
    using IpcPacketParserPtr = std::shared_ptr<IpcPacketParser>;

public:
    /** set a special client id for output message by current parser,
     *  if not set this client id, then will be the client id parsed from received data
     */
    IpcPacketParser(DataReceiveHandler on_receive, ClientId client_id = INVALID_CLIENT_ID);
    ~IpcPacketParser();

public:
    core::IcppErrc fillData(const uint8_t* data, uint32_t size);
private:
    const uint8_t* onParseStart(const uint8_t* data_ptr, const uint8_t* end_ptr);
    const uint8_t* onParseHeader(const uint8_t* data_ptr, const uint8_t* end_ptr);
    const uint8_t* onParsePayload(const uint8_t* data_ptr, const uint8_t* end_ptr);    
    const uint8_t* onParseEnd(const uint8_t* data_ptr, const uint8_t* end_ptr);

private:   
    ClientId client_id_;
    IpcDataParseState parse_state_ = IpcDataParseState::Start;
    uint32_t header_read_pos_ = 0;
    uint32_t payload_read_pos_ = 0;
    uint32_t payload_size_ = 0;
    DataReceiveHandler on_receive_;
    
    Message::MessagePtr msg_ptr_ = nullptr;
    PayloadPtr payload_ptr_ = nullptr;

    uint8_t header_buf_[IPC_PKT_HDR_SIZE];
    
    
};


/** from communication message build a ipc packet */
class IpcPacketBuilder final
{
public:
    static PayloadPtr MakeIpcPacket(Message::MessagePtr msg_ptr);
};





} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_COM_IPC_PACKET_H__ */

