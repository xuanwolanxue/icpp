/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: ipc_endpoint_client.h
 *
 * Purpose: implementation client endpoint for ipc
 *
 * Developer:
 *   wen.gu , 2021-10-12
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_COM_IPC_ENDPOINT_CLIENT_H__
#define __ICPP_COM_IPC_ENDPOINT_CLIENT_H__

#include "icpp/com/endpoint.h"
#include "ipc_socket.h"
#include "ipc_packet.h"
/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{

/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/

class IpcEndpointClient: public Endpoint
{
public:
    IpcEndpointClient(const std::string& url);
    virtual ~IpcEndpointClient();
public:
    /**initialize endpoint. 
     * if role service, then  do  communication protocol bind(e.g. socket bind)
     * if role client, then try to connect to service endpoint
     */
    virtual IcppErrc start() override;

    /** uninitialize endpoint
     * if role service, then do unbind
     * if role client,  then disconect from service endpoint
    */
    virtual IcppErrc stop() override;

    /** the common api */
    /**
     * endpoint_id:   == INVALID_ENDPOINT_ID:   (default)broadcast to all endpoints which connected with current endpoint, 
     *                != INVALID_ENDPOINT_ID: send to a special endpoint  which connected with current endpoint
     */
    virtual IcppErrc send(Message::MessagePtr msg_ptr) override;
    
    virtual void setReceiveHandler(ReceiveHandler handler) override;
    virtual void setConnectionStateHandler(ConnectionStateHandler handler) override;
    virtual void setEndpointIdAllocator(EndpointIdAllocator handler) override;
public:
    virtual EndpointRole role() const override;
    //virtual EndpointId endpoint_id() override;
    virtual const std::string& url() const  override;

private:
    //ClientId client_id_ = INVALID_CLIENT_ID; /** here indicate target endpoint id(the service endpoint id, a fake value) */
    IpcPacketParser::IpcPacketParserPtr on_packet_parse_ = nullptr;
    std::string srv_url_;
    IpcSocketClient ipc_client_;
    //ReceiveHandler on_receive_;
    ConnectionStateHandler on_connect_state_change_;
    EndpointIdAllocator on_endpoint_id_allocator_;    
};

} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_COM_IPC_ENDPOINT_CLIENT_H__ */

