/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: ipc_endpoint.cpp
 *
 * Purpose: implementation a endpoint plugin with ipc
 *
 * Developer:
 *   wen.gu , 2021-10-12
 *
 * TODO:
 *
 ***************************************************************************/

 /******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#include "icpp/core/plugin.h"
#include "ipc_endpoint_client.h"
#include "ipc_endpoint_service.h"

#include "icpp/core/utils.h"

#define LOG_TAG "ipce"
#include "icpp/core/log.h"

namespace icpp
{
namespace com
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/

/******************************************************************************
 **    VARIABLE DEFINITIONS
 ******************************************************************************/
class IpcEndpointPlugin: public core::Plugin
{
public:
    IpcEndpointPlugin(const std::string& name, const std::string& version,  const std::string& author)
    :core::Plugin(name, version, author)
    {
        /** todo something */
    }
    ~IpcEndpointPlugin() { /** todo something */ }
public:

    /** create a component instance(real plugin must implement this API)
     *@parameter

     * [in] param the parameter to request the component
     * [io] comp  the pointer point to component instance,if failed, then nullptr
     *
     *@return
     *   success: IcppErrc::OK, failed: error code.
     */
    core::IcppErrc createComponent(const core::ComponentCreateParam* param, core::Component** comp) override;
};
/******************************************************************************
 **    FUNCTION DEFINITIONS
 ******************************************************************************/

core::IcppErrc IpcEndpointPlugin::createComponent(const core::ComponentCreateParam* param, core::Component** comp) 
{
    const EndpointCreateParam* ecp = static_cast<const EndpointCreateParam*>(param); 

    if (!ecp || !comp)
    {
        return core::IcppErrc::BadParameter;
    }       

    if (core::StringStartWith(ecp->url, "ipc://"))
    {
        if (ecp->role == EndpointRole::kService)
        {
            *comp = new IpcEndpointService(ecp->url.substr(6)); /**6: skip prefix: "ipc://" */
        }
        else if (ecp->role == EndpointRole::kClient)
        {
           // *comp = new IpcEndpointClient();
        }
        else
        {
            return core::IcppErrc::UnsupportedType;
        }

        return core::IcppErrc::OK;
    }

    return core::IcppErrc::UnsupportedType;
}


} /** namespace com */
} /** namespace icpp */


PLUGIN_ENTRY(icpp::com::IpcEndpointPlugin, 
            "Ipc Endpoint Plugin",
            "00.01.00",
            "guwen");

