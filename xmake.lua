
includes("check_links.lua")
includes("check_ctypes.lua")
includes("check_cflags.lua")
includes("check_cfuncs.lua")

add_includedirs("include")
set_languages("cxx11")

set_configdir("$(buildir)/config")
add_configfiles("config.h.in")

configvar_check_links("HAS_POSIX_THREAD", {"pthread", "m", "dl"})
configvar_check_cfuncs("HAS_POSIX_FILE_API", {"open", "close", "write", "read", "lseek", "fsync", "fstat"}, {includes = {"fcntl.h", "unistd.h", "sys/stat.h"}})
configvar_check_cfuncs("HAS_POSIX_DIR_API", {"opendir","closedir", "readdir", "rmdir", "mkdir", "access"}, {includes = {"dirent.h", "unistd.h", "sys/stat.h"}})
configvar_check_cfuncs("HAS_POSIX_DLL_API", {"dlopen", "dlclose", "dlsym", "dlerror"}, {includes = {"dlfcn.h"}})
add_includedirs("$(buildir)/config")

-- add rules: debug/release
add_rules("mode.debug", "mode.release")

add_subdirs("source/core",
			"source/executor",
			"source/com",
			"plugins"
           )
		   
		   