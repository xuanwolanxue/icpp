/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: thread_posix.cpp
 *
 * Purpose: thread implementation for posix.
 *
 * Developer:
 *   wen.gu , 2021-04-02
 *
 * TODO:
 *
 ***************************************************************************/

 /******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#include "config.h"

#if defined(HAS_POSIX_THREAD) && (HAS_POSIX_THREAD)
#include "icpp/executor/thread.h"
#include <pthread.h>
#include <sys/prctl.h>


namespace icpp
{
namespace executor
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/

/******************************************************************************
 **    VARIABLE DEFINITIONS
 ******************************************************************************/

class Thread::Impl
{
public:
    int32_t priority_ = -1;
    std::thread* thd_ = nullptr;
    std::string name_;
    ThreadRunnable runnable_;

};

// Min/max thread priority range for QNX
static const int32_t MIN_THREAD_PRIORITY = 8;
static const int32_t MAX_THREAD_PRIORITY = 22;

/******************************************************************************
 **    FUNCTION DEFINITIONS
 ******************************************************************************/

static bool SetThreadPriority(pthread_t thd, int32_t priority)
{
    bool status(false);

    int32_t schedPolicy;
    struct sched_param schedParam;
    if (!pthread_getschedparam(thd, &schedPolicy, &schedParam))
    {
        schedParam.sched_priority = priority;
        if (!pthread_setschedparam(thd, schedPolicy, &schedParam))
        {
            status = true;
        }
    }

    return status;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
Thread::Thread(const std::string& name, ThreadRunnable runnable)
    :impl_(new Impl)
{
    impl_->name_ = name;
    impl_->runnable_ = runnable;
    /** todo something */
}

Thread::~Thread()
{
    /** todo something */
}

bool Thread::start()
{
    if (!impl_->thd_)
    {
        impl_->thd_ = new std::thread(impl_->runnable_);
        return true;
    }

    return false;
}

void Thread::join()
{
    if (impl_->thd_)
    {
        impl_->thd_->join();
    }
}

void Thread::detach()
{
    if (impl_->thd_)
    {
        impl_->thd_->detach();
    }
}

bool Thread::set_priority(int32_t priority)
{ 
    if (impl_->thd_)
    {
        if ((Thread::priority_min() <= priority) && (priority <= Thread::priority_max()))
        {
            if (SetThreadPriority(impl_->thd_->native_handle(), priority))
            {
                impl_->priority_ = priority;
                return true;
            }
        }
    }
        
    return false;
}

bool Thread::set_name(const std::string& name)
{   
    impl_->name_ = name;
    if (impl_->thd_)
    {
        if (isSelf())
        {
            prctl(PR_SET_NAME, name.c_str());
        }        
        else
        {
            pthread_setname_np(impl_->thd_->native_handle(), name.c_str());
        }

        
        return true;
    }

    return false;
}


bool Thread::joinable() const
{
    return impl_->thd_ ? impl_->thd_->joinable() : false;
}

bool Thread::isSelf() const
{
    return impl_->thd_ ? (impl_->thd_->get_id() == std::thread::id()) : false;
}

int32_t Thread::priority() const
{
    return impl_->priority_;
}

const std::string& Thread::name() const
{
    return impl_->name_;
}


//static 
int32_t Thread::priority_max()
{
    return MAX_THREAD_PRIORITY;    
}

//static 
int32_t Thread::priority_min()
{
    return MIN_THREAD_PRIORITY;
}

} /** namespace executor */
} /** namespace icpp */


#endif