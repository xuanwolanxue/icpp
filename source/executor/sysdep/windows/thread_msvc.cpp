/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: thread_msvc.cpp
 *
 * Purpose: thread implementation for windows platform.
 *
 * Developer:
 *   wen.gu , 2021-04-02
 *
 * TODO:
 *
 ***************************************************************************/

 /******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#if defined(_MSC_VER)
#include "icpp/executor/thread.h"

#define _CRT_SECURE_NO_DEPRECATE
#include <windows.h>
#include <winbase.h>
#include <process.h>
#include <stdio.h>

#pragma comment(lib, "winmm.lib")

namespace icpp
{
namespace executor
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/

/******************************************************************************
 **    VARIABLE DEFINITIONS
 ******************************************************************************/
 // The information on how to set the thread name comes from
 // a MSDN article: http://msdn2.microsoft.com/en-us/library/xcb2z8hs.aspx
static const DWORD kVCThreadNameException = 0x406D1388;

typedef struct tagTHREADNAME_INFO {
    DWORD dwType;  // Must be 0x1000.
    LPCSTR szName;  // Pointer to name (in user addr space).
    DWORD dwThreadID;  // Thread ID (-1=caller thread).
    DWORD dwFlags;  // Reserved for future use, must be zero.
} THREADNAME_INFO;



class Thread::Impl
{
public:
    std::thread* thd_ = nullptr;
    std::string name_;
    ThreadRunnable runnable_;

};
/******************************************************************************
 **    FUNCTION DEFINITIONS
 ******************************************************************************/


// This function has try handling, so it is separated out of its caller.
static bool SetNameInternal(HANDLE thread_id, const char* name)
{
    //只在调试的时候生效 
    if (!::IsDebuggerPresent())
        return false;
    THREADNAME_INFO info;
    info.dwType = 0x1000;
    info.szName = name;
    info.dwThreadID = (DWORD)thread_id;
    info.dwFlags = 0;

    __try
    {
        RaiseException(kVCThreadNameException, 0, sizeof(info) / sizeof(DWORD), reinterpret_cast<DWORD_PTR*>(&info));
    }
    __except (EXCEPTION_CONTINUE_EXECUTION)
    {
        return false;
    }

    return true;
}



///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
Thread::Thread(const std::string& name, ThreadRunnable runnable)
    :impl_(new Impl)
{
    impl_->name_ = name;
    impl_->runnable_ = runnable;
    /** todo something */
}

Thread::~Thread()
{
    /** todo something */
}

bool Thread::start()
{
    if (!impl_->thd_)
    {
        impl_->thd_ = new std::thread(impl_->runnable_);
        set_name(impl_->name_);
        return true;
    }

    return false;
}

void Thread::join()
{
    if (impl_->thd_)
    {
        impl_->thd_->join();
    }
}

void Thread::detach()
{
    if (impl_->thd_)
    {
        impl_->thd_->detach();
    }
}

bool Thread::set_priority(int32_t priority)
{
    if ((Thread::priority_min() <= priority) && (priority <= Thread::priority_max()))
    {
        return impl_->thd_ ? SetThreadPriority(impl_->thd_->native_handle(), priority) : false;
    }
    
    return false;
}

bool Thread::set_name(const std::string& name)
{
    if (impl_->thd_)
    {
        bool ret = SetNameInternal(impl_->thd_->native_handle(), name.c_str());
        if (ret)
        {
            impl_->name_ = name;
        }

        return ret;
    }
    return false;
}


bool Thread::joinable() const
{
    return impl_->thd_ ? impl_->thd_->joinable() : false;
}

bool Thread::isSelf() const
{
    return impl_->thd_ ? (impl_->thd_->get_id() == std::thread::id()) : false;
}

int32_t Thread::priority() const
{
    return impl_->thd_ ? GetThreadPriority(impl_->thd_->native_handle()) : 0;
}

const std::string& Thread::name() const
{
    return impl_->name_;
}


//static 
int32_t Thread::priority_max()
{
    return THREAD_BASE_PRIORITY_MAX;
}

//static 
int32_t Thread::priority_min()
{
    return THREAD_BASE_PRIORITY_MIN;
}

} /** namespace executor */
} /** namespace icpp */


#endif /** define _MSC_VER */