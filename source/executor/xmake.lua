target("icpp_executor")
    set_kind("shared")	
	if is_plat("windows") then
		add_defines("EXECUTOR_DLL_EXPORT")
	end
    add_files("./*.cpp", "./sysdep/*/*.cpp")
	add_deps("icpp_core")
	add_links("icpp_core")