target("icpp_com")
    set_kind("shared")	
	if is_plat("windows") then
		add_defines("COM_DLL_EXPORT")
	end
    add_files("./*.cpp")
	add_deps("icpp_core", "icpp_executor")
	add_links("icpp_core", "icpp_executor")