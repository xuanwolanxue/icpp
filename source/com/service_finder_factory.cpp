/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: service_finder_factory.cpp
 *
 * Purpose: implementation a factory to create sercice finder.
 *
 * Developer:
 *   wen.gu , 2021-09-30
 *
 * TODO:
 *
 ***************************************************************************/

 /******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#include "icpp/com/service_finder_factory.h"

#include <map>
#include "registration_center_service_finder.h"


#define LOG_TAG "srff"
#include "icpp/core/log.h"
namespace icpp
{
namespace com
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/
#define IPC_SRV_URL_PREFIX "ipc:///temp/icpp_service."
/******************************************************************************
 **    VARIABLE DEFINITIONS
 ******************************************************************************/
class ServiceFinderFactory::Impl
{
public:
    using FactoryMap = std::map<ServiceFinderType, MakeServiceFinder>;
public:
    FactoryMap finder_makers_;
};
/******************************************************************************
 **    FUNCTION DEFINITIONS
 ******************************************************************************/
ServiceFinderFactory::ServiceFinderFactory()
    :impl_(new Impl)
{
    /** todo something */
    /** add default finder new handler */
    impl_->finder_makers_[ServiceFinderType::kRegistrationCenter] = [](const std::string& srv_name) -> ServiceFinderPtr
    {
        return std::make_shared<RegistrationCenterServiceFinder>(srv_name);
    };
}

ServiceFinderFactory::~ServiceFinderFactory()
{
    /** todo something */
}


bool ServiceFinderFactory::addFactroy(ServiceFinderType finder_type, MakeServiceFinder on_new_finder)
{
    impl_->finder_makers_[finder_type] = on_new_finder;
    return true; /** todo, refine me ?? */
}

//static 
ServiceFinderFactory& ServiceFinderFactory::getInstance()
{
    static ServiceFinderFactory sff;
    return sff;
}

//static 
ServiceFinderFactory::ServiceFinderPtr ServiceFinderFactory::create(ServiceFinderType finder_type, const std::string& srv_name)
{
    ServiceFinderFactory& sff = getInstance();
    Impl::FactoryMap::iterator it = sff.impl_->finder_makers_.find(finder_type);

    if (it != sff.impl_->finder_makers_.end())
    {
        if (it->second)
        {
            return it->second(srv_name);
        }
    }

    return nullptr;
}


} /** namespace com */
} /** namespace icpp */
