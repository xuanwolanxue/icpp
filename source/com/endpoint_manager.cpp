/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.sys>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: endpoint_manager.cpp
 *
 * Purpose: endpoint manager implementation
 *
 * Developer:
 *   wen.gu , 2021-08-20
 *
 * TODO:
 *
 ***************************************************************************/

 /******************************************************************************
 **    INCLUDES
 ******************************************************************************/

#include "icpp/com/endpoint_manager.h"

#include "icpp/core/plugin_manager.h"

#define LOG_TAG "edpm"
#include "icpp/core/log.h"

namespace icpp
{
namespace com
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    VARIABLE DEFINITIONS
 ******************************************************************************/



/******************************************************************************
 **    FUNCTION DEFINITIONS
 ******************************************************************************/
EndpointManager::EndpointManager()
{
    /** todo something */
}

EndpointManager::~EndpointManager()
{
    /** todo something */
}


Endpoint* EndpointManager::find(const std::string& url, EndpointRole role)
{
    icpp::core::PluginManager& pm = icpp::core::PluginManager::getInstance();
    EndpointCreateParam ecp;
    ecp.url = url;
    ecp.role = role;
    Endpoint* ep = nullptr;
    IcppErrc ret = pm.find(&ecp, (icpp::core::Component**)&ep);

    if (IcppErrc::OK != ret)
    {
        LOGE("find endpoint component for url: %s, as role: %d failed(%s)\n",url.c_str(), (int32_t)role, icpp::core::ErrorStr(ret));
    }

    return ep;
}


}
}