/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.sys>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: deserializer_default_impl.cpp
 *
 * Purpose: default implementation for deserializer
 *
 * Developer:
 *   wen.gu , 2021-05-10
 *
 * TODO:
 *
 ***************************************************************************/

 /******************************************************************************
 **    INCLUDES
 ******************************************************************************/

#include "icpp/com/deserializer.h"

#include <string.h>

namespace icpp
{
namespace com
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    VARIABLE DEFINITIONS
 ******************************************************************************/



/******************************************************************************
 **    FUNCTION DEFINITIONS
 ******************************************************************************/
Deserializer::Deserializer(const uint8_t* data, uint32_t length, uint32_t start_pos /*= 0*/)
    :payload_(std::make_shared<Payload>(data, data+length)),
    pos_(start_pos)
{
    /** todo something */
}

Deserializer::Deserializer(PayloadPtr payload, uint32_t start_pos /*= 0*/)
    :payload_(payload),
    pos_(start_pos)
{
    /** todo something */
}

Deserializer::~Deserializer()
{
    /** todo something */
}
bool Deserializer::deserialize(bool& value)
{
    return deserialize_l((uint8_t*)&value, sizeof(value));
}

bool Deserializer::deserialize(uint8_t& value)
{
    return deserialize_l((uint8_t*)&value, sizeof(value));
}

bool Deserializer::deserialize(uint16_t& value)
{
    return deserialize_l((uint8_t*)&value, sizeof(value));
}

bool Deserializer::deserialize(uint32_t& value)
{
    return deserialize_l((uint8_t*)&value, sizeof(value));
}

bool Deserializer::deserialize(uint64_t& value)
{
    return deserialize_l((uint8_t*)&value, sizeof(value));
}

bool Deserializer::deserialize(int8_t& value)
{
    return deserialize_l((uint8_t*)&value, sizeof(value));
}

bool Deserializer::deserialize(int16_t& value)
{
    return deserialize_l((uint8_t*)&value, sizeof(value));
}

bool Deserializer::deserialize(int32_t& value)
{
    return deserialize_l((uint8_t*)&value, sizeof(value));
}

bool Deserializer::deserialize(int64_t& value)
{
    return deserialize_l((uint8_t*)&value, sizeof(value));
}

bool Deserializer::deserialize(float& value)
{
    return deserialize_l((uint8_t*)&value, sizeof(value));
}

bool Deserializer::deserialize(double& value)
{
    return deserialize_l((uint8_t*)&value, sizeof(value));
}

bool Deserializer::deserialize(std::string& value)
{
    uint32_t str_length = 0;

    if (!deserialize(str_length))
    {
        return false;
    }

    if (str_length > 0)
    {
        if ((pos_ + str_length) > payload_->size())
        {
            return false;
        }

        value.clear();
        value.append((const char*)(payload_->data() + pos_), str_length);
        pos_ += str_length;        
    }

    return true;
}

bool Deserializer::deserialize(Deserializable& deser)
{
    return deser.deserialize(this);
}

bool Deserializer::deserialize_l(uint8_t* data, uint32_t size)
{
    if ((pos_ + size) <= payload_->size())
    {
        memcpy(data, payload_->data() + pos_, size);
        pos_ += size;
        return true;
    }

    return false;
}

bool Deserializer::deserialize()
{
    return true;
}


bool Deserializer::setNewPayload(PayloadPtr payload_ptr, uint32_t start_pos/* = 0*/)
{
    payload_ = payload_ptr;
    pos_ = start_pos;
    return true;
}

} /** namespace com */
} /** namespace icpp */

