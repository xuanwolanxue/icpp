/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.sys>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: serializer_default_impl.cpp
 *
 * Purpose: default implementation for serilaizer
 *
 * Developer:
 *   wen.gu , 2021-05-10
 *
 * TODO:
 *
 ***************************************************************************/

 /******************************************************************************
 **    INCLUDES
 ******************************************************************************/

#include "icpp/com/serializer.h"

namespace icpp
{
namespace com
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    VARIABLE DEFINITIONS
 ******************************************************************************/



/******************************************************************************
 **    FUNCTION DEFINITIONS
 ******************************************************************************/
Serializer::Serializer()
:payload_(std::make_shared<Payload>(DEFAULT_BUF_SIZE))
{
    /** todo, something */
}

Serializer::~Serializer()
{
    /** todo, something */
}

bool Serializer::serialize(bool val)
{
    return serialize_l((const uint8_t*)&val, sizeof(val));
}

bool Serializer::serialize(int8_t val)
{
    return serialize_l((const uint8_t*)&val, sizeof(val));
}

bool Serializer::serialize(int16_t val)
{
    return serialize_l((const uint8_t*)&val, sizeof(val));
}

bool Serializer::serialize(int32_t val)
{
    return serialize_l((const uint8_t*)&val, sizeof(val));
}

bool Serializer::serialize(int64_t val)
{
    return serialize_l((const uint8_t*)&val, sizeof(val));
}

bool Serializer::serialize(uint8_t val)
{
    return serialize_l((const uint8_t*)&val, sizeof(val));
}

bool Serializer::serialize(uint16_t val)
{
    return serialize_l((const uint8_t*)&val, sizeof(val));
}

bool Serializer::serialize(uint32_t val)
{
    return serialize_l((const uint8_t*)&val, sizeof(val));
}

bool Serializer::serialize(uint64_t val)
{
    return serialize_l((const uint8_t*)&val, sizeof(val));
}

bool Serializer::serialize(float val)
{
    return serialize_l((const uint8_t*)&val, sizeof(val));
}

bool Serializer::serialize(double val)
{
    return serialize_l((const uint8_t*)&val, sizeof(val));
}

bool Serializer::serialize(const std::string& val)
{
    if (serialize((uint32_t)val.size()))
    {
        if (val.size() > 0)
        {
            return serialize_l((const uint8_t*)val.c_str(), uint32_t(val.size()));
        }
        
        return true;
    }

    return false;
}

bool Serializer::serialize(const Seriablizable& ser)
{
    return ser.serialize(this);
}

bool Serializer::serialize_l(const uint8_t* data, uint32_t length)
{
    if (payload_)
    {
        payload_->insert(payload_->end(), data, data+length);
        
        return true;
    }

    return false;
}

bool Serializer::serialize()
{
    return true;
}

bool Serializer::reset()
{
    payload_ = std::make_shared<Payload>(DEFAULT_BUF_SIZE);
    return true;
}

} /** namespace com */
} /** namespace icpp */


