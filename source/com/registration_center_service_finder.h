/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: registration_center_service_finder.h
 *
 * Purpose: implementation a service finder by registration center client
 *
 * Developer:
 *   wen.gu , 2021-09-30
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_COM_REGISTRATION_CENTER_SERVICE_FINDER_H__
#define __ICPP_COM_REGISTRATION_CENTER_SERVICE_FINDER_H__

#include "icpp/com/service_finder.h"
#include "icpp/com/registration_center_client.h"
#include "icpp/com/response_builder.h"

/******************************************************************************
 **    MACROS
 ******************************************************************************/

/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{



/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/

class RegistrationCenterServiceFinder: public ServiceFinder
{
public:
    using IcppErrc = core::IcppErrc;
public:
    RegistrationCenterServiceFinder(const std::string& srv_name);
    ~RegistrationCenterServiceFinder();
public:
    /**
     * try to find a service instance, and return result immediately.
    */
    Result<ServiceInfo> tryFindService() override;

    /**
     * start to find a service instance and wait unitil timeout or successfull find a valid service instance
     */
    Result<ServiceInfo> findService(uint32_t wait_ms = FIND_SERVICE_WAIT_INFINITE) override;

    /**
     * cancel find service  when 'findService' method called and in waiting state. 
     */
    Result<void> cancelFindService() override;

    /**
     * start to find a service with async mode, the result will return by callback  'handler'
     */
    Result<void> startFindServiceAsync(OnFindServiceHandler handler) override;

     /**
     * stop to find a service with async mode
     */   
    Result<void> stopFindServiceAsync() override;

private:
    bool is_wait_ = false;
    uint32_t listener_id_for_wait_ = INVALID_LISTENER_ID;
    uint32_t listener_id_for_async_ = INVALID_LISTENER_ID;
    std::string srv_name_;
    std::mutex lock_;
    ResponseBuilder<ServiceInfo>::ResponseBuilderPtr res_builder_for_wait_ = nullptr;
    //RegistrationCenterClient reg_center_;
};




} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_COM_REGISTRATION_CENTER_SERVICE_FINDER_H__ */

