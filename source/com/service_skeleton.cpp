/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: service_skeleton.cpp
 *
 * Purpose: service skeleton
 *
 * Developer:
 *   wen.gu , 2021-08-05
 *
 * TODO:
 *
 ***************************************************************************/

 /******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#include "icpp/com/service_skeleton.h"

#include "icpp/com/service_provider.h"

#define LOG_TAG "sklt"
#include "icpp/core/log.h"
namespace icpp
{
namespace com
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/
#define IPC_SRV_URL_PREFIX "ipc:///temp/icpp_service."
/******************************************************************************
 **    VARIABLE DEFINITIONS
 ******************************************************************************/
class ServiceSkeleton::Impl
{
public:
    uint8_t interface_version_;
    std::string service_name_;
    ServiceProvider provider_;
};
/******************************************************************************
 **    FUNCTION DEFINITIONS
 ******************************************************************************/

ServiceSkeleton::ServiceSkeleton(const std::string& service_name, uint8_t interface_version /*= 0*/)
    :impl_(new Impl)
{
    /** todo something */
    impl_->interface_version_ = interface_version;
    impl_->service_name_ = service_name;
}

ServiceSkeleton::~ServiceSkeleton()
{
    /** todo something */
}


ServiceSkeleton::IcppErrc ServiceSkeleton::offerService()
{
    ServiceProviderInitializeParam init_param;
    init_param.interface_version = impl_->interface_version_;
    init_param.service_name = impl_->service_name_;
    init_param.urls.push_back(IPC_SRV_URL_PREFIX + impl_->service_name_); /** todo refine me ??*/
    init_param.on_state_change = nullptr; /** is need observe the state change of connection to current service */
    IcppErrc ret = impl_->provider_.initialize(init_param);

    if (IcppErrc::OK != ret)
    {
        LOGE("initialize service provider failed: %s\n", core::ErrorStr(ret));
        return ret;
    }

    return impl_->provider_.offerService();

}

ServiceSkeleton::IcppErrc ServiceSkeleton::stopService()
{
    return impl_->provider_.stopService();        
}

ServiceSkeleton::MessagePtr ServiceSkeleton::newMessage(MessageType type, MessageId message_id, PayloadPtr payload_ptr)
{
    return impl_->provider_.newMessage(type, message_id, payload_ptr);
}

ServiceSkeleton::SerializerPtr ServiceSkeleton::newSerializer()
{
    return impl_->provider_.newSerializer();
}

ServiceSkeleton::DeserializerPtr ServiceSkeleton::newDeserializer(PayloadPtr payload)
{
    return impl_->provider_.newDeserializer(payload);
}

ServiceSkeleton::IcppErrc ServiceSkeleton::sendMessage(MessagePtr msg_ptr)
{
    return impl_->provider_.sendMessage(msg_ptr);
}

/** type maybe: requst, no return request, property set/get */
ServiceSkeleton::IcppErrc ServiceSkeleton::registerMessageHandler(MessageType type, MessageId method_id, MessageHandler method)
{
    return impl_->provider_.registerMessageHandler(type, method_id, method);
}


} /** namespace com */
} /** namespace icpp */
