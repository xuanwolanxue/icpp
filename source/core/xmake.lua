target("icpp_core")
    set_kind("shared")
	if is_plat("windows") then
		add_defines("CORE_DLL_EXPORT")
	end
    add_files("./*.cpp", "./sysdep/*/*.cpp")
	