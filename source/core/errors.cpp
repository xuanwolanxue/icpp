/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: errors.cpp
 *
 * Purpose: PandaOS base error code define and helper API implementation.
 *
 * Developer:
 *   wen.gu , 2020-05-11
 *
 * TODO:
 *
 ***************************************************************************/

 /******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#include "icpp/core/errors.h"


namespace icpp
{
namespace core
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/
#define ERR_CASE(err) case err: ret = #err; break

/******************************************************************************
 **    VARIABLE DEFINITIONS
 ******************************************************************************/



/******************************************************************************
 **    FUNCTION DEFINITIONS
 ******************************************************************************/
const char* ErrorStr(IcppErrc errc)
{
    char const* ret = "unknown";
    switch (errc)
    {
    ERR_CASE(IcppErrc::OK);
    ERR_CASE(IcppErrc::Fail);
    ERR_CASE(IcppErrc::BadParameter);
    ERR_CASE(IcppErrc::NoInit);
    ERR_CASE(IcppErrc::Eof);
    ERR_CASE(IcppErrc::NotImplemented);
    ERR_CASE(IcppErrc::NotFound);
    ERR_CASE(IcppErrc::Undefined);
    ERR_CASE(IcppErrc::InvaildName);
    ERR_CASE(IcppErrc::InsufficientResources);
    ERR_CASE(IcppErrc::InvalidStatus);
    ERR_CASE(IcppErrc::OutOfRange);
    ERR_CASE(IcppErrc::UnsupportedType);
    ERR_CASE(IcppErrc::Timeout);
    ERR_CASE(IcppErrc::BadAddress);
    ERR_CASE(IcppErrc::ConnectionAborted); /**< connection aborted(ECONNABORTED) */
    ERR_CASE(IcppErrc::ConnectionAlreadyInProgress); /**< connection already in progress(EALREADY) */
    ERR_CASE(IcppErrc::ConnectionRefused); /**< connection refused(ECONNREFUSED) */
    ERR_CASE(IcppErrc::ConnectionReset); /**< connection reset(ECONNRESET) */
    ERR_CASE(IcppErrc::BadMessage);
    ERR_CASE(IcppErrc::OperationCanceled); /**< operation canceled(ECANCELED) */
    ERR_CASE(IcppErrc::OperationInProgress); /**< operation in progress(EINPROGRESS) */
    ERR_CASE(IcppErrc::OperationNotPermitted); /**< operation not permitted(EPERM) */
    ERR_CASE(IcppErrc::OperationNotSupported); /**< operation not supported(EOPNOTSUPP) */
    ERR_CASE(IcppErrc::OperationWouldBlock); /**< operation would block(EWOULDBLOCK) */
    ERR_CASE(IcppErrc::OutOfMemory);
    ERR_CASE(IcppErrc::AlreadyExisted);
    ERR_CASE(IcppErrc::ResourceIsBusy);
    ERR_CASE(IcppErrc::SerializeFailed);
    ERR_CASE(IcppErrc::DeserializeFailed);
    default: break;
    }

    return ret;
}

#if 1
void Error2Json(IcppErrc err, Value& val)
{
    val["code"] = (int32_t)err;
    val["description"] = ErrorStr(err);
}

IcppErrc Json2Error(const Value& val)
{
    IcppErrc ret = IcppErrc::Undefined;

    if (val.isMember("code"))
    {
        ret = (IcppErrc)val["code"].asInt();
    }

    return ret;
}
#endif

} /** namespace core */
} /** namespace icpp */


