/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: log.cpp
 *
 * Purpose: log system implementation
 *
 * Developer:
 *   wen.gu , 2019-07-03
 *
 * TODO:
 *
 ***************************************************************************/

 /******************************************************************************
 **    INCLUDES
 ******************************************************************************/

#include "icpp/core/log.h"

#include <stdarg.h>
#include <vector>
#include <stdio.h>
#include <sstream>
#include <ctime>


namespace icpp
{
namespace core
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/

/******************************************************************************
 **    VARIABLE DEFINITIONS
 ******************************************************************************/



/******************************************************************************
 **    FUNCTION DEFINITIONS
 ******************************************************************************/

class Logger::impl
{
public:
    bool mEnableTimestamp = false;
    std::string mModuleName = "icpp"; /** like "MF" */
    LogCallbackFunc mCallback = nullptr;
    /** only logPrint level <= mLevel can be log output */
    LogLevel mLevel = LogLevel::kDebug;
    char mTimestampBuf[128] = { 0 };

public:
    /** if not set observer, then this function will be called print to std */
    void defaultPrint(LogLevel mll,
                      const char* tag,
                      const char* func,
                      int line,
                      const char* str);
};



void Logger::impl::defaultPrint(LogLevel mll, const char* tag,
    const char* func, int line, const char* str)
{
    if (str)
    {
        char* buf = new char[512];
        Logger::getInstance().makeLogPrefix(buf, 511, mll, tag, func, line);

        printf("%s%s\n", buf, str);

        delete[] buf;
    }
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

Logger::Logger()
    :impl_(new impl)
{
    //todo something
}

Logger::~Logger()
{
    //todo something
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//static
Logger& Logger::getInstance()
{
    static Logger ml;

    return ml;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void Logger::logPrint(LogLevel mll, 
                      const char* tag, 
                      const char * func, 
                      int line, 
                      const char * fmt, ...)
{   
    if (mll <= impl_->mLevel)
    {
        if (fmt)
        {
            va_list args;
            va_start(args, fmt);

            /** process args */   
            int32_t len = std::vsnprintf(nullptr, 0, fmt, args);
            va_end(args);   /** todo refine me?? */

            if (len > 0)
            {
                char* tempBuf = new char[len + 1];
                va_start(args, fmt);
                std::vsnprintf(tempBuf, len, fmt, args);
                va_end(args);


                if (impl_->mCallback)
                {
                    impl_->mCallback(mll, tag, func, line, tempBuf);
                }
                else
                {
                    impl_->defaultPrint(mll, tag, func, line, tempBuf);
                }  
                
                delete[] tempBuf;
            }            
        }
    }    
}        

const char* Logger::logLevel2ShortStr(LogLevel mll)
{
    const char* type = "unknown";
    switch(mll)
    {
    case LogLevel::kError: type   = "ERR"; break;
    case LogLevel::kDebug: type   = "DBG"; break;
    case LogLevel::kInfo:  type   = "INF"; break;
    case LogLevel::kWarning: type = "WRN"; break;
    }

    return type;
}

void Logger::makeTimestampStr(char buf[], size_t len)
{    
    if (impl_->mEnableTimestamp)
    {
        std::time_t t = std::time(nullptr);
        std::strftime(buf, len, "%D %T", std::gmtime(&t));
    }
    else
    {
        buf[0] = '\0';
    }    
}

void Logger::makeLogPrefix(char buf[], size_t len, /** output buffer */
                         LogLevel mll, const char* tag,
                         const char* func, int line)
{

    char timestampBuf[128];
    const char* type = logLevel2ShortStr(mll);
    /** process format param */
    makeTimestampStr(timestampBuf, sizeof(timestampBuf) - 1);

    std::snprintf(buf, len, "%s[%s][%s][%s.%d][%s]:", 
                 timestampBuf, impl_->mModuleName.c_str(),
                 tag, func, line, type); 
}                             

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void Logger::initialize(LogLevel defaultLevel,
                        ConstString& moduleName,
                        LogCallbackFunc cbFunc /*= nullptr*/)
{
    impl_->mLevel = defaultLevel;
    impl_->mCallback = cbFunc;
    impl_->mModuleName = moduleName;
}
                          
  
void Logger::setDefaultLevel(LogLevel mll)
{
    impl_->mLevel = mll;
}

LogLevel Logger::getDefaultLevel()
{
    return impl_->mLevel;
}


ConstString& Logger::getModuleName()
{
    return impl_->mModuleName;
}

void Logger::enableTimestamp(bool enable)
{
    impl_->mEnableTimestamp = enable;
}

} /** namespace core */
} /** namespace icpp */
