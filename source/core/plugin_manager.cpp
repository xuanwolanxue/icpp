/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: plugin_manager.cpp
 *
 * Purpose: plugin manager implementation
 *
 * Developer:
 *   wen.gu , 2019-08-18
 *
 * TODO:
 *
 ***************************************************************************/

 /******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#include "icpp/core/plugin_manager.h"


#include <map>
#include <list>

#include "icpp/core/plugin.h"    
#include "icpp/core/shared_library.h"
#include "icpp/core/errors.h"

#define LOG_TAG "plgm"
#include "icpp/core/log.h"

namespace icpp
{
namespace core
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/

/******************************************************************************
 **    VARIABLE DEFINITIONS
 ******************************************************************************/

struct PluginHandle
{
    bool freeable = true;
    void* dll_handle = nullptr;
    Plugin* plugin = nullptr;
    std::string name;    
};

using PluginMap = std::map<Plugin*, PluginHandle>;
using PluginArray = std::list<PluginHandle>;
/******************************************************************************
 **    FUNCTION DEFINITIONS
 ******************************************************************************/
 class PluginManager::impl
{
public:
     IcppErrc load_l(const std::string& name);
     bool libIsLoaded(const std::string& name);

public:
     //PluginMap plugins_;
     PluginArray plugins_;
     std::mutex lock_;
};


 IcppErrc PluginManager::impl::load_l(const std::string& name)
 {
     if (libIsLoaded(name) == false)
     {
         SharedLibHandle dll_handle = SharedLibOpen(name.c_str());

         if (dll_handle != INVALID_LIB_HANDLE)
         {
             pluginEntryFunc_t pluginEntry =
                 (pluginEntryFunc_t)SharedLibSym(dll_handle, PLUGIN_ENTRY_NAME);
             pluginHdrVerFunc_t verFunc =
                 (pluginHdrVerFunc_t)SharedLibSym(dll_handle, PLUGIN_VER_FUNC_NAME);

             if (pluginEntry && verFunc)
             {
                 const std::string ver = verFunc();

                 if (ver == PLUGIN_HEADER_VER)
                 {/** todo, process the compatibility of master version */
                     Plugin* mp = pluginEntry();
                     
                     if (mp == nullptr)
                     {
                         return IcppErrc::InsufficientResources;
                     }

                     PluginHandle mph;
                     mph.dll_handle = dll_handle;
                     mph.name = name;
                     mph.plugin = mp;
                     plugins_.push_back(mph);

                     //plugins_[mp] = mph;
                     LOGD("load plugin(%s, ver: %s, author: %s) success "
                         "with plugin interface ver:%s\n",
                         mp->name().c_str(), mp->version().c_str(),
                         mp->author().c_str(), PLUGIN_HEADER_VER);
                     return IcppErrc::OK;
                 }
                 else
                 {
                     LOGE("version mismatch, host(%s), plugin(%s)\n",
                         PLUGIN_HEADER_VER, ver.c_str());
                     return IcppErrc::Undefined;
                 }
             }
             else
             {
                 LOGE("lib(%s) haven't entry func(%s, %s)\n",
                     name.c_str(), PLUGIN_ENTRY_NAME,
                     PLUGIN_VER_FUNC_NAME);

                 return IcppErrc::Undefined;
             }
         }
         else
         {
             LOGE("load lib(%s) failed(%s)\n", name.c_str(), SharedLibError().c_str());
         }
     }
     else
     {
         LOGE("lib(%s) already loaded\n", name.c_str());
     }

     return IcppErrc::InvalidStatus;
 }

 bool PluginManager::impl::libIsLoaded(const std::string& name)
 {
     bool ret = false;
     PluginArray::iterator it = plugins_.begin();

     for (; it != plugins_.end(); it++)
     {
         if (name == it->name)
         {
             ret = true;
             break;
         }
     }

     return ret;
 }


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
 PluginManager::PluginManager()
     :impl_(new impl)
 {
     /**todo, something */
 }

 PluginManager::~PluginManager()
 {
     unloadAll();
     /**todo, something */
 }

 //static
 PluginManager& PluginManager::getInstance()
 {
     static PluginManager mpm;
     return mpm;
 }


 IcppErrc PluginManager::load(const std::string& name)
 {
     //AutoLock al(lock_);
     return impl_->load_l(name);
 }

 IcppErrc PluginManager::loadList(std::vector<std::string>& pluginList,
     bool ignoreError/* = true*/)
 {
     IcppErrc ret = IcppErrc::OK;
     uint32_t loadCnt = 0;
     //AutoLock al(lock_);

     for (uint32_t i = 0; i < pluginList.size(); i++)
     {
         ret = impl_->load_l(pluginList[i]);

         if (bool(ret))
         {
             loadCnt++;
         }
         else if (ignoreError)
         {
             ret = IcppErrc::OK;
         }
         else
         {
             LOGE("load plugin(%s) failed(%d, %s)\n", pluginList[i].c_str(), ret, ErrorStr(ret));
             break;
         }
     }

     LOGD("loaded plugin(%d)\n", loadCnt);

     return ret;
 }


 IcppErrc PluginManager::unloadAll()
 {
     //AutoLock al(lock_);

     PluginArray::iterator it = impl_->plugins_.begin();

     for (; it != impl_->plugins_.end(); it++)
     {
         if (it->plugin && it->freeable)
         {
             delete it->plugin;
         }

         if (it->dll_handle)
         {
             SharedLibClose(it->dll_handle);
         }
     }

     impl_->plugins_.clear();

     return IcppErrc::OK;
 }

IcppErrc PluginManager::find(const ComponentCreateParam* param, Component** comp)
 {
    IcppErrc ret = IcppErrc::NotFound;
     //AutoLock al(lock_);
     PluginArray::iterator it = impl_->plugins_.begin();

     for (; it != impl_->plugins_.end(); it++)
     {
         Plugin* mp = it->plugin;
         if (mp)
         {
             LOGD("===>(type: %s, plugin: %s)\n",
                 param->type.c_str(), mp->name().c_str());

             ret = mp->createComponent(param, comp);

             //LOGD("===>ret(%d, %s)\n", ret.code, ret.desc);
             if (ret == IcppErrc::OK)
             {
                 LOGD("create component instance success, name: %s, version: %s\n",
                     (*comp)->name().c_str(), (*comp)->version().c_str());
                 break;
             }
         }
     }

     return ret;
 }


 IcppErrc PluginManager::registerPlugin(Plugin* plugin, bool freeable /*= true*/)
 {
     if (plugin == nullptr)
     {
         return IcppErrc::BadParameter;
     }

    PluginHandle mph;
    mph.freeable = freeable;
    mph.plugin = plugin;    

    impl_->plugins_.push_back(mph);

    return IcppErrc::OK;
 }


} /** namespace core */
} /** namespace icpp */
