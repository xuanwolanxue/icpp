/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: ini_manager.cpp
 *
 * Purpose: configure manager(ini file) imeplementation
 *
 * Developer:
 *   wen.gu , 2018-12-11
 *
 * TODO:
 *
 ***************************************************************************/

 /******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#include "icpp/core/ini_configure.h"
#include "icpp/core/ini_file.h"
#include "icpp/core/errors.h"

namespace icpp
{
namespace core
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/

/******************************************************************************
 **    VARIABLE DEFINITIONS
 ******************************************************************************/



/******************************************************************************
 **    FUNCTION DEFINITIONS
 ******************************************************************************/
class IniConfigure::impl
{
public:
    impl();
    ~impl();
public:
    IniFile* mIniF = nullptr;
};

IniConfigure::impl::impl()
{
    mIniF = new IniFile;
}

IniConfigure::impl::~impl()
{
    if (mIniF)
    {
        delete mIniF;
    }
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

IniConfigure::IniConfigure()
    :impl_(new impl)
{
    /** todo something */
}

IniConfigure::~IniConfigure()
{
    /** todo something */
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

bool IniConfigure::load(ConstString& fileName)
{
    IcppErrc ret = impl_->mIniF->load(fileName);

    return bool(ret);
}

bool IniConfigure::getValue(ConstString& group, 
                                ConstString& key, 
                                std::string& val)
{
    IcppErrc ret = impl_->mIniF->getValue(group, key, val);

    return bool(ret);
}   

bool IniConfigure::getValue(ConstString& group, 
                                ConstString& key, 
                                int32_t& val)
{
    std::string str;
    bool ret = getValue(group, key, str);

    if (ret)
    {
        val = atoi(str.c_str());
    }

    return ret;
}                                         
                           
bool IniConfigure::getValues(ConstString& group, 
                                 ConstString& key, 
                                 std::vector<std::string>& vals)
{
    IcppErrc ret = impl_->mIniF->getValues(group, key, vals);

    return ret == IcppErrc::OK;
} 

} /** namespace core */
} /** namespace icpp */
