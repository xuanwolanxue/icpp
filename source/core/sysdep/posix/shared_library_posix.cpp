/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: shared_library_posix.cpp
 *
 * Purpose: shared library operation api posix implementation
 *
 * Developer:
 *   wen.gu , 2019-07-04
 *
 * TODO:
 *
 ***************************************************************************/

 /******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#include "config.h"
#if defined(HAS_POSIX_DLL_API) && (HAS_POSIX_DLL_API) 
#include "icpp/core/shared_library.h"

#include <dlfcn.h>


namespace icpp
{
namespace core
{
 /******************************************************************************
 **    MACROS
 ******************************************************************************/


 /******************************************************************************
 **    VARIABLE DEFINITIONS
 ******************************************************************************/



 /******************************************************************************
 **    FUNCTION DEFINITIONS
 ******************************************************************************/

SharedLibHandle SharedLibOpen(ConstString& libPath)
{
    return dlopen(libPath.c_str(), RTLD_LAZY);
}

void  SharedLibClose(SharedLibHandle slHdl)
{
    dlclose(slHdl);
}

void* SharedLibSym(SharedLibHandle slHdl, ConstString& sym)
{
    return dlsym(slHdl, sym.c_str());
}

std::string SharedLibError()
{
    return std::string(dlerror());
}

} /** namespace core */
} /** namespace icpp */

#endif /** end of __GNUC__ */
