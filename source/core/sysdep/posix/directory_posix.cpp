/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: directory_posix.cpp
 *
 * Purpose: directory posix implmentation
 *
 * Developer:
 *   wen.gu , 2019-10-15
 *
 * TODO:
 *
 ***************************************************************************/

 /******************************************************************************
 **    INCLUDES
 ******************************************************************************/

#include "config.h"

#if defined(HAS_POSIX_DIR_API) && HAS_POSIX_DIR_API
  
#include "icpp/core/directory.h"
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>

namespace icpp
{
namespace core
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/
#ifndef F_OK
#define F_OK 0
#endif
/******************************************************************************
 **    VARIABLE DEFINITIONS
 ******************************************************************************/



/******************************************************************************
 **    FUNCTION DEFINITIONS
 ******************************************************************************/
class Directory::impl
{
public:
    DIR* mDirHdl = nullptr;
    std::string mCurDir = "";
    bool mIsDir = false;
public:
    const std::string& next()
    {
        do
        {
            struct dirent* pDir = readdir(mDirHdl);
            if (pDir)
            {
                mCurDir = pDir->d_name;
                mIsDir = (pDir->d_type == DT_DIR);
            }                
            else
            {
                mCurDir.clear();
            }
                
        } while ((mCurDir == ".") || (mCurDir == ".."));

        return mCurDir;
    }
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

std::shared_ptr<Directory> Directory::OpenDir(const std::string& dir)
{
    std::shared_ptr<Directory> pDir = nullptr;

    if (Directory::IsAccessale(dir))
    {
#if 0
        pDir = std::make_shared<Directory>();
        pDir->impl_->mDirHdl = opendir(dir.c_str());

        if (pDir->impl_->mDirHdl)
        {
            pDir->impl_->next();
        }
        else
        {
            pDir = nullptr;
        }
#endif
    }

    return pDir;
}

//static 
bool Directory::IsAccessale(const std::string& path)
{
    return (access(path.c_str(), F_OK) == 0);
}

//static 
int Directory::MkDir(const std::string& dir)
{
    return mkdir(dir.c_str(), S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
}

//static 
int Directory::RmDir(const std::string& dir)
{
    return rmdir(dir.c_str());
}


Directory::~Directory()
{
    /** todo something */
    if (impl_->mDirHdl)
    {
        closedir(impl_->mDirHdl);
        impl_->mDirHdl = nullptr;
    }
}


Directory::Directory()
    :impl_(new impl)
{
    /** todo something */
}


/** isDir: true: directory, false: file */
bool Directory::foreach(void(*dirFunc)(std::string& path, bool isDir, void* opaque), void* opaque)
{
    if (dirFunc)
    {
        std::string& curDir = impl_->mCurDir;
        bool& isDir = impl_->mIsDir;

        while (!curDir.empty())
        {
            dirFunc(curDir, isDir, opaque);
            impl_->next();
        }

        return true;
    }

    return false;
}

} /** namespace core */
} /** namespace icpp */

#endif /** defined HAVE_POSIX_DIR_API */
