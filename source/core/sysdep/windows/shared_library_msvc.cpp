/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: shared_library_msvc.cpp
 *
 * Purpose: shared library operation api msvc implementation
 *
 * Developer:
 *   wen.gu , 2019-07-04
 *
 * TODO:
 *
 ***************************************************************************/

 /******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#if defined(_MSC_VER)
#include "icpp/core/shared_library.h"

#include <windows.h>

namespace icpp
{
namespace core
{
 /******************************************************************************
 **    MACROS
 ******************************************************************************/


 /******************************************************************************
 **    VARIABLE DEFINITIONS
 ******************************************************************************/

 /******************************************************************************
 **    FUNCTION DEFINITIONS
 ******************************************************************************/
SharedLibHandle SharedLibOpen(const std::string& libPath)
{
    SharedLibHandle dllHd = INVALID_LIB_HANDLE;
#ifdef UNICODE
    size_t len = libPath.size();
    size_t mallocSize = (size_t)(len * (sizeof(wchar_t)) + (sizeof(wchar_t)));
    wchar_t *wdmname = (wchar_t *)malloc(mallocSize);

    if (!wdmname)
    {
        return nullptr;
    }
    memset(wdmname, 0, mallocSize);

    if (mbstowcs(wdmname, (const char*)libPath.c_str(), (size_t)len) == -1)
    {
        free(wdmname);
        return nullptr;
    }

    dllHd = LoadLibrary((LPWSTR)wdmname);
    free(wdmname);
#else
    dllHd = LoadLibrary((LPCSTR)libPath.c_str());
#endif
    return dllHd;
}

void  SharedLibClose(SharedLibHandle slHdl)
{
    FreeLibrary((HMODULE)slHdl);
}

void* SharedLibSym(SharedLibHandle slHdl, ConstString& sym)
{
    return GetProcAddress((HMODULE)slHdl, (LPCSTR)sym.c_str());
}

std::string SharedLibError()
{
    DWORD ErrorCode = GetLastError();
    LPVOID LocalAddress = NULL;
    FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_FROM_SYSTEM,
        NULL, ErrorCode, 0, (LPTSTR)&LocalAddress, 0, NULL);
    return std::string((const char*)LocalAddress);
}

} /** namespace core */
} /** namespace icpp */

#endif /** end of _MSC_VER */