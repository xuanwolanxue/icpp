/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: directory.cpp
 *
 * Purpose: directory wrapper for msvc implementation
 *
 * Developer:
 *   wen.gu , 2019-10-15
 *
 * TODO:
 *
 ***************************************************************************/

 /******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#include "icpp/core/directory.h"

#if defined(_MSC_VER)
#include <chrono>
#include <thread>     
#include <Windows.h>
#include <io.h>
#include <direct.h> 

#include "icpp/core/utils.h"
#define LOG_TAG "drms"
#include "icpp/core/log.h"

namespace icpp
{
namespace core
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/
#ifndef F_OK
#define F_OK 0
#endif

#ifdef UNICODE 
#define MY_DOT L"."
#define MY_DOUABLE_DOT L".."
#else
#define MY_DOT "."
#define MY_DOUABLE_DOT ".."
#endif
/******************************************************************************
 **    VARIABLE DEFINITIONS
 ******************************************************************************/



/******************************************************************************
 **    FUNCTION DEFINITIONS
 ******************************************************************************/
class Directory::impl
{
public:
    HANDLE mDirHdl = INVALID_HANDLE_VALUE;
    WIN32_FIND_DATA mDirFd;
    std::string mCurrentDir = "";
    bool mCurIsDir = false;
public:
    const std::string& next()
    {
        do
        {
            if (FindNextFile(mDirHdl, &mDirFd) != 0)
            {
#ifdef UNICODE
                mCurrentDir = WString2String(mDirFd.cFileName);
#else
                mCurrentDir = mDirFd.cFileName;
#endif
                mCurIsDir = (mDirFd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY);
            }                 
            else
            {
                mCurrentDir.clear();
            }                
        } while (mCurrentDir == "." || mCurrentDir == "..");

        return mCurrentDir;
    }
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

//static 
std::shared_ptr<Directory> Directory::OpenDir(const std::string& dir)
{
    if (Directory::IsAccessale(dir))
    {
        std::shared_ptr<Directory> pdir(new Directory);

        HANDLE& hdl = pdir->impl_->mDirHdl;
        WIN32_FIND_DATA& fd = pdir->impl_->mDirFd;

        std::string& curDir = pdir->impl_->mCurrentDir;
#ifdef UNICODE
        const std::wstring tagDir = String2WString(dir);
#else
        const std::string & tagDir = dir;
#endif

        hdl = FindFirstFile(tagDir.c_str(), &fd);
        if (hdl == INVALID_HANDLE_VALUE)
        {
            if (GetLastError() != ERROR_NO_MORE_FILES)
            {
                curDir.clear();
                LOGE("open dir(%s) failed(%d)\n", tagDir.c_str(), GetLastError());
            }
        }
        else
        {
#ifdef UNICODE
            curDir = WString2String(fd.cFileName);
#else
            curDir = fd.cFileName;
#endif
            if ((curDir == ".") || (curDir == ".."))
            {
                pdir->impl_->next();
            }                
        }

        if (curDir.empty())
        {/** if empty, mean that open dir failed */
            pdir = nullptr;
        }

        return pdir;
    }

    return nullptr;
}

//static 
bool Directory::IsAccessale(const std::string& path)
{
    return (_access(path.c_str(), F_OK) == 0);
}

//static 
int Directory::MkDir(const std::string& dir)
{
    return _mkdir(dir.c_str());
}

//static 
int Directory::RmDir(const std::string& dir)
{
    return _rmdir(dir.c_str());
}


Directory::~Directory()
{
    /** todo something */
    if (impl_->mDirHdl != INVALID_HANDLE_VALUE)
    {
        FindClose(impl_->mDirHdl);
        impl_->mDirHdl = INVALID_HANDLE_VALUE;
    }
}


Directory::Directory()
    :impl_(new impl)
{
    /** todo something */
}


/** isDir: true: directory, false: file */
bool Directory::foreach(void(*dirFunc)(std::string& path, bool isDir, void* opaque), void* opaque)
{
    if (dirFunc)
    {
        std::string& curDir = impl_->mCurrentDir;
        bool& isDir = impl_->mCurIsDir;

        while (curDir.empty() != false)
        {  
            dirFunc(curDir, isDir, opaque);
            impl_->next();
        }

        return true;
    }

    return false;
}

} /** namespace core */
} /** namespace icpp */

#endif /** defined _MSC_VER */
