/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: executor.h
 *
 * Purpose: Executor implmentation
 *
 * Developer:
 *   wen.gu , 2021-03-27
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __EXECUTOR_EXECUTOR_H__
#define __EXECUTOR_EXECUTOR_H__

#include <string>
#include <functional>
#include <future>
#include <type_traits>

#include "icpp/executor/types.h"

namespace icpp
{
namespace executor
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/

/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/

/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/
/**
 * the task tyep:
 * - normal task
 * - urgent task.
 *
 * Executor aways check and run urgent task at first. then after check and run
 * noramal task.
 */

class EXECUTOR_CLASS Executor final
{
public:
    using Task = std::function<void()>;

public:
    /**
     * the queue size if set as 0, then indicat that there is no limit for the size of queue.
     */
    Executor(const std::string& name, uint32_t task_queue_size = 0, uint32_t urgent_task_queue_size = 0);
    ~Executor();
public:
    bool start();
    bool stop();
     
    bool set_priority(int32_t priority);
    bool set_name(const std::string& name);

    int32_t priority() const;
    const std::string& name() const;
public:
    /**
     * @brief add task to executor
     * 
     * @urgent true: add task to urgent task queue, false add task to normal task queue
     *
     * @return true: submit success, false: failed, e.g. max queue size set, and the task queue is full.
     * @note The executor have 2 task queue, urgent and normal. The executor aways query urgent 
     *       queue at first, normal queue at second.
     */
    bool submitTask(Task&& task, bool urgent = false);

private:
    class Impl;
    std::unique_ptr<Impl> impl_;
};

} /** namespace executor */
} /** namespace icpp */

#endif /** !__EXECUTOR_EXECUTOR_H__ */

