/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: timer.h
 *
 * Purpose: a soft timer implmentation with thread.
 *
 * Developer:
 *   wen.gu , 2021-03-27
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __EXECUTER_TIMER_H__
#define __EXECUTER_TIMER_H__

#include <string>
#include <functional>
#include <memory>

#include "icpp/executor/types.h"

namespace icpp
{
namespace executor
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/

/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/
class EXECUTOR_CLASS Timer final
{
public:
    using TimerHandler = std::function<void()>;
public:
    /*
     * Create a timer
     *
     * @param [in] start_interval_ms: how many time delay before timer first start.
     * @param [in] periodic_interval_ms: the timeout interval of timer; > 0: timer is cyclic, = 0: only run once. 
     * @return None
     */
    Timer(uint32_t start_interval_ms = 0, uint32_t periodic_interval_ms = 0);
    ~Timer();
public:

    bool start(TimerHandler handler);
    bool start(TimerHandler handler, int32_t priority);

    bool stop();

    /**
     * Restarts the periodic interval. If the callback method is already running, nothing will happen.
     */
    bool restart();

    /**
     * Sets a new periodic interval and restarts the timer. An interval of 0 will stop the timer.
     */
    bool restart(uint32_t periodic_interval_ms);

public:
    uint32_t start_interval() const;
    uint32_t periodic_interval() const;

    /**
     * Sets the start interval. Will only be effective before start() is called.
     */
    void set_start_interval(uint32_t start_interval_ms);

    /**
     * Sets the periodic interval. If the timer is already running the new interval will be 
     * effective when the current interval expires.
     */
    void set_periodic_interval(uint32_t periodic_interval_ms);

    /**
     * Returns the number of skipped invocations since the last invocation.
	 * Skipped invocations happen if the timer callback(TimerHandler) function takes
	 * longer to execute than the timer interval(periodic interval).
     */
    uint32_t skipped() const;

private:
    class Impl;
    std::unique_ptr<Impl> impl_;
};
} /** namespace executor */
} /** namespace icpp */

#endif /** !__EXECUTER_TIMER_H__ */

