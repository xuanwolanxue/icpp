/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: watchdog.h
 *
 * Purpose: a software watchdog implmentation with thread.
 *
 * Developer:
 *   wen.gu , 2021-03-27
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __EXECUTER_WATCHDOG_H__
#define __EXECUTER_WATCHDOG_H__

#include <memory>
#include <functional>

#include "icpp/executor/types.h"

namespace icpp
{
namespace executor
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/

/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/
class EXECUTOR_CLASS Watchdog final
{
public:
    /** current handler just called once, when watchdog timeout occur */
    using BarkHandler = std::function<void()>;
public:
    Watchdog();
    ~Watchdog();
public:
    /**
     * counter_ms: the time of watchdog timeout
     */
    bool enable(uint32_t counter_ms); /** when this funcation called, watchdog start to counter */
    bool disable();
    bool feedDog();

    bool bark(); /** is watchdog timeout */

    /**
     * Sets the bark handler. Will only be effective before enable() is called.
     */
    void setBarkHandler(BarkHandler handler);
private:
    class Impl;
    std::unique_ptr<Impl> impl_;

};
} /** namespace executor */
} /** namespace icpp */

#endif /** !__EXECUTER_WATCHDOG_H__ */

