/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: thread.h
 *
 * Purpose: a portable thread wrapper
 *
 * Developer:
 *   wen.gu , 2021-03-25
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __EXECUTER_THREAD_H__
#define __EXECUTER_THREAD_H__

#include <string>
#include <functional>
#include <thread>
#include "icpp/executor/types.h"


/******************************************************************************
 **    MACROS
 ******************************************************************************/

/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/

/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace executor
{

class EXECUTOR_CLASS Thread final
{
public:
    /**
     * return: true: keep running, false: exit thread loop.
     */
    using ThreadRunnable = std::function<bool()>;
public:
    Thread(const std::string& name, ThreadRunnable runnable);
    ~Thread();
public:
    bool start();    
    void join();
    void detach();

    bool set_priority(int32_t priority);
    bool set_name(const std::string& name);

public:
    bool joinable() const;
    bool isSelf() const;
    int32_t priority() const;
    const std::string& name() const;

public:
    static int32_t priority_max();
    static int32_t priority_min();

private:
    class Impl;
    std::unique_ptr<Impl> impl_;
};

} /** namespace executor */
} /** namespace icpp */

#endif /** !__EXECUTER_THREAD_H__ */

