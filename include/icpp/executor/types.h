/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
  * Name: types.h
  *
  * Purpose: base types
  *
  * Developer:
  *   wen.gu , 2019-07-03
  *
  * TODO:
  *
  ***************************************************************************/

  /******************************************************************************
   **    INCLUDES
   ******************************************************************************/
#ifndef __ICPP_EXECUTOR_H__
#define __ICPP_EXECUTOR_H__

#include "icpp/core/types.h"
/******************************************************************************
 **    MACROS
 ******************************************************************************/
#ifdef __cplusplus
#define EXECUTOR_EXTERN_C extern "C"
#else
#define EXECUTOR_EXTERN_C 
#endif


#if defined(_MSC_VER) /** !!!for visual studio */
    ////////////////////////////////////////////////////////////////////////////
    /** compat the keyword 'inline' */
#if (_MSC_VER > 1200) && !defined (__cplusplus)
#define inline    _inline  /** to compat keyword 'inline' */
#endif

#ifndef DLL_EXPORT_TAG
#define DLL_EXPORT_TAG EXECUTOR_DLL_EXPORT
#endif

#if defined (EXECUTOR_DLL_EXPORT)
#define EXECUTOR_API_EXPORT __declspec(dllexport)
#else
#define EXECUTOR_API_EXPORT

#endif
#else /** for other toolchain */
#define EXECUTOR_API_EXPORT 
#endif

    /** general api decorate macro */
#define EXECUTOR_API EXECUTOR_API_EXPORT /**c++ api */

#define EXECUTOR_API_C EXECUTOR_EXTERN_C EXECUTOR_API_EXPORT /** c api */

/** general class decorate macro*/
#define EXECUTOR_CLASS EXECUTOR_API_EXPORT

/** general global variable decorate macro(for external reference ) */
#define EXECUTOR_VAR  extern EXECUTOR_API_EXPORT 

/** general const global variable decorate macro(for external reference ) */
#define EXECUTOR_CVAR EXECUTOR_VAR const 



 // Conditional NORETURN attribute on the throw functions would:
 // a) suppress false positives from static code analysis
 // b) possibly improve optimization opportunities.
#if !defined(EXECUTOR_NO_RETURN)
#if defined(_MSC_VER)
#define EXECUTOR_NO_RETURN __declspec(noreturn)
#elif defined(__GNUC__)
#define EXECUTOR_NO_RETURN __attribute__((__noreturn__))
#else
#define EXECUTOR_NO_RETURN
#endif
#endif


/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace executor 
{


/******************************************************************************
**    CLASSES/FUNCTIONS DEFINITIONS
******************************************************************************/

} /** namespace executor */
} /** namespace icpp */


#endif /** !__ICPP_EXECUTOR_H__ */
