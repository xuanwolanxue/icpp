/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: types.h
 *
 * Purpose: communication base type define
 *
 * Developer:
 *   wen.gu , 2021-06-15
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_COM_TYPES_H__
#define __ICPP_COM_TYPES_H__
#include <stdint.h>
#include <vector>
#include <memory>

#include "icpp/core/types.h"
#include "icpp/core/errors.h"
/******************************************************************************
 **    MACROS
 ******************************************************************************/

#ifdef __cplusplus
#define COM_EXTERN_C extern "C"
#else
#define COM_EXTERN_C 
#endif


#if defined(_MSC_VER) /** !!!for visual studio */
    ////////////////////////////////////////////////////////////////////////////
    /** compat the keyword 'inline' */
#if (_MSC_VER > 1200) && !defined (__cplusplus)
#define inline    _inline  /** to compat keyword 'inline' */
#endif

#ifndef DLL_EXPORT_TAG
#define DLL_EXPORT_TAG COM_DLL_EXPORT
#endif

#if defined (COM_DLL_EXPORT)
#define COM_API_EXPORT __declspec(dllexport)
#else
#define COM_API_EXPORT

#endif
#else /** for other toolchain */
#define COM_API_EXPORT 
#endif

    /** general api decorate macro */
#define COM_API COM_API_EXPORT /**c++ api */

#define COM_API_C COM_EXTERN_C COM_API_EXPORT /** c api */

/** general class decorate macro*/
#define COM_CLASS COM_API_EXPORT

/** general global variable decorate macro(for external reference ) */
#define COM_VAR  extern COM_API_EXPORT 

/** general const global variable decorate macro(for external reference ) */
#define COM_CVAR COM_VAR const 



 // Conditional NORETURN attribute on the throw functions would:
 // a) suppress false positives from static code analysis
 // b) possibly improve optimization opportunities.
#if !defined(COM_NO_RETURN)
#if defined(_MSC_VER)
#define COM_NO_RETURN __declspec(noreturn)
#elif defined(__GNUC__)
#define COM_NO_RETURN __attribute__((__noreturn__))
#else
#define COM_NO_RETURN
#endif
#endif

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/** for 'id' */
#define INVALID_ENDPOINT_ID (nullptr)
#define INVALID_MESSAGE_ID  (0xffff)
#define INVALID_EVENT_ID    (0xffff)
#define INVALID_PROPERTY_ID (0xffff)
#define INVALID_SERVICE_ID  INVALID_ENDPOINT_ID
#define INVALID_CLIENT_ID   INVALID_ENDPOINT_ID
#define INVALID_SESSION_ID  0
//#define 

/** for protocol version define */
#define COM_PROTOCOL_VERSION 0x01
/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{

using EndpointId = uintptr_t;
using ServiceId  = uint16_t;
using ClientId   = uint16_t;
using SessionId  = uint16_t;
using MessageId  = uint16_t;


using Payload    = std::vector<uint8_t>;
using PayloadPtr = std::shared_ptr<Payload>;
using UrlList    = std::vector<std::string>;

using MethodId   = MessageId;
using EventId    = MessageId;
using PropertyId = MessageId;
/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/




} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_COM_TYPES_H__ */

