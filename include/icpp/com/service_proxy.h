/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: service_proxy.h
 *
 * Purpose: proxy base class define for service
 *
 * Developer:
 *   wen.gu , 2021-09-28
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_COM_SERVICE_PROXY_H__
#define __ICPP_COM_SERVICE_PROXY_H__
#include <functional>
#include <vector>
#include <memory>

#include "icpp/com/types.h"
#include "icpp/com/service_types.h"
#include "icpp/com/message.h"
#include "icpp/com/message_broker.h"
#include "icpp/com/response.h"
#include "icpp/com/result.h"
#include "icpp/com/service_finder.h"
/******************************************************************************
 **    MACROS
 ******************************************************************************/
/** for sync connect operation, wait forever until connect to service */
#define CONNECT_WAIT_INFINITE (-1)

/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{

class ServiceHandleType
{
public:
    using MessageBrokerPtr = MessageBroker::MessageBrokerPtr;
    using ServiceFinderPtr = ServiceFinder::ServiceFinderPtr;
public:
    ServiceHandleType();
    virtual ~ServiceHandleType();
public:
    ServiceHandleType(const ServiceHandleType& other);
    ServiceHandleType(ServiceHandleType&& other);
    ServiceHandleType& operator=(const ServiceHandleType& other);
    ServiceHandleType& operator=(ServiceHandleType&& other);
public:
    virtual MessageBrokerPtr broker();

protected:
    friend class ServiceProxy;
    class Impl;
    std::unique_ptr<Impl> impl_;
};
/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/
class COM_CLASS ServiceProxy
{
public:

    using IcppErrc = core::IcppErrc;
    using MessagePtr = Message::MessagePtr;
    
    using SerializerPtr = Serializer::SerializerPtr;
    using DeserializerPtr = Deserializer::DeserializerPtr;
    using MessageHandler = std::function<void(MessagePtr)>;

    /**bool: true: async connect to service successfull, false: failed */
    using ConnectHandler = std::function<void(bool)>;
    using EventHandler = MessageHandler;
    using ServiceProxyPtr = std::shared_ptr<ServiceProxy>;
    using MessageBrokerPtr = MessageBroker::MessageBrokerPtr;
public:
    ServiceProxy(ServiceFinder::ServiceFinderPtr service_finder, uint8_t interface_version = 0);
    virtual ~ServiceProxy();
public:
    /**
     * start to connect to service and wait unitil timeout or successfull connect to service
     */
    virtual IcppErrc connect(int32_t timeout_ms = CONNECT_WAIT_INFINITE);

    /**
     * try to connect to service, and return result immediately.
    */
    virtual IcppErrc tryConnect();

    /**
     * cancel connect  when Connect method called and in waiting state. 
     */
    virtual void cancelConnect();

    /** disconnect from service*/
    virtual IcppErrc disconnect();

    /**
     * start to connect to service, the result will return by callback  'handler'
     */
    virtual IcppErrc startConnectAsync(ConnectHandler handler);
    virtual IcppErrc stopConnectAsync();    

public:
    virtual MessageBroker::MessageBrokerPtr broker();

private:
    class Impl;
    std::unique_ptr<Impl> impl_;
};

} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_COM_SERVICE_PROXY_H__ */

