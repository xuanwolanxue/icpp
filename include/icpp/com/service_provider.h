/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: service_provider.h
 *
 * Purpose: service provicer base class define
 *
 * Developer:
 *   wen.gu , 2021-06-15
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_SERVICE_PROVIDER_H__
#define __ICPP_SERVICE_PROVIDER_H__
#include <functional>

#include "icpp/com/types.h"
#include "icpp/com/service_types.h"
#include "icpp/com/message.h"
#include "icpp/com/message_broker.h"
#include "icpp/com/std_apply.h"
/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{


using ServiceStateChangedHandler = std::function<void(ServiceActivityStatus)>;


struct ServiceProviderInitializeParam
{
    uint8_t interface_version = 0;
    ServiceId service_id = INVALID_SERVICE_ID;
    std::string service_name;
    ServiceStateChangedHandler on_state_change;
    UrlList urls;  /** if not input urls for service, then ServiceProvider will be alloc urls from registration center */

};
/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/

class COM_CLASS ServiceProvider
{
public:
    using IcppErrc = core::IcppErrc;
    using MessagePtr = Message::MessagePtr;
    using SerializerPtr = Serializer::SerializerPtr;
    using DeserializerPtr = Deserializer::DeserializerPtr;
    using MessageBrokerPtr = MessageBroker::MessageBrokerPtr;
    using MessageHandler = MessageBroker::MessageHandler;    
public:
    ServiceProvider();
    virtual ~ServiceProvider();

public:
    virtual IcppErrc initialize(const ServiceProviderInitializeParam& param);    
    virtual IcppErrc offerService();
    virtual IcppErrc stopService();    
public:
    virtual IcppErrc sendMessage(MessagePtr msg_ptr);

    /** the type only could be: notification and property notify */
    virtual IcppErrc notify(MessageType type, MessageId message_id, PayloadPtr payload_ptr);

public:
    /** type maybe: requst, no return request, property set/get */
    virtual IcppErrc registerMessageHandler(MessageType type, MessageId method_id, MessageHandler method);

public: /** for message: create, serialize, deserialize */
    
    virtual MessagePtr newMessage(MessageType type, MessageId message_id, PayloadPtr payload_ptr);

    virtual SerializerPtr newSerializer();
    virtual DeserializerPtr newDeserializer(PayloadPtr payload);

public:
    virtual const std::string& service_name() const { return service_info_.service_name; }
protected:
    MessageBrokerPtr msg_broker_ptr_;
    ServiceInfo service_info_;

};


} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_SERVICE_PROVIDER_H__ */

