/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: serializer.h
 *
 * Purpose: serializer base class implementation
 *
 * Developer:
 *   wen.gu , 2021-05-10
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_COM_SERIALIZER_H__
#define __ICPP_COM_SERIALIZER_H__

#include <vector>
#include <string>
#include <array>
#include <map>
#include <stdint.h>
#include <memory>
#include <utility>

#include "icpp/com/types.h"
#include "icpp/com/serializable.h"

/******************************************************************************
 **    MACROS
 ******************************************************************************/
//the default size of serializer buffer
#define DEFAULT_BUF_SIZE (256)

/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{


/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/
class COM_CLASS Serializer: public std::enable_shared_from_this<Serializer>
{
public:
    using SerializerPtr = std::shared_ptr<Serializer>;
public:
    Serializer();
    virtual ~Serializer();
public:
    virtual PayloadPtr payload() const { return payload_;}

public:
    virtual bool serialize(bool val);
    virtual bool serialize(int8_t val);
    virtual bool serialize(int16_t val);
    virtual bool serialize(int32_t val);
    virtual bool serialize(int64_t val);
    virtual bool serialize(uint8_t val);
    virtual bool serialize(uint16_t val);
    virtual bool serialize(uint32_t val);
    virtual bool serialize(uint64_t val);
    virtual bool serialize(float val);
    virtual bool serialize(double val);
    virtual bool serialize(const std::string& val);
    virtual bool serialize(const Seriablizable& ser);
public:

    template<typename T, typename std::enable_if<!(std::is_integral<T>::value ||  std::is_floating_point<T>::value) >::type>
    bool serialize(const std::vector<T>& val)
    {/** treat vector as dynamic array, |val.size()| element values...| */

        if (!serialize((uint32_t)val.size()))
        {
            return false;
        }

        for (auto it : val)
        {
            if (!serialize(it))
            {
                return false;
            }
        }

        return true;
    }

    template<typename T, typename std::enable_if<(std::is_integral<T>::value ||  std::is_floating_point<T>::value) >::type >
    bool serialize(const std::vector<T>& val)
    {/** treat vector as dynamic array, |val.size()| element values...| */
        if (!serialize((uint32_t)val.size()))
        {
            return false;
        }

        return serialize_l((const uint8_t*)val.data(), val.size() * sizeof(T));        
    }


    template<typename T, std::size_t N, typename std::enable_if<!(std::is_integral<T>::value ||  std::is_floating_point<T>::value) >::type>
    bool serialize(const std::array<T, N>& val)
    {
        if (!serialize((uint32_t)val.size()))
        {
            return false;
        }

        for (auto it : val)
        {
            if (!serialize(it))
            {
                return false;
            }
        }

        return true;
    }

    template<typename T, std::size_t N, typename std::enable_if<(std::is_integral<T>::value ||  std::is_floating_point<T>::value) >::type>
    bool serialize(const std::array<T, N>& val)
    {
        if (!serialize((uint32_t)val.size()))
        {
            return false;
        }

        return serialize_l((const uint8_t*)val.data(), val.size() * sizeof(T)); 
    }


    template<typename T, typename std::enable_if<std::is_enum<T>::value >::type >
    bool serialize(T val) /** serialize for enum */
    {
        static_assert(sizeof(val) < 8, "the enum only uint32_t");

        return serialize((uint32_t)val); /** the enum all as uint32_t. so */
    }

    template<typename Key, typename Value>
    bool serialize(const std::map<Key, Value>& val)
    {
        if (!serialize((uint32_t)val.size()))
        {
            return false;
        }

        for (auto it : val)
        {
            if (!serialize(it->first) || !serialize(it->second))
            {
                return false;
            }
        }

        return true;
    }

    template<typename T, typename ...Args>
    bool serialize(const T& t, const Args&... args)
    {
        if (!serialize(std::forward<const T&>(t)))
        {
            return false;
        }

        return serialize(std::forward<const Args&>(args)...);
    }
public:
    /** reset the state of serializer,*/
    virtual bool reset(); 
protected:
    virtual bool serialize_l(const uint8_t* data, uint32_t length);
    virtual bool serialize(); /** do nothing, just for end the  bool serialize(const T& t, const Args&... args) */
protected:
    PayloadPtr payload_;


};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_COM_SERIALIZER_H__ */





