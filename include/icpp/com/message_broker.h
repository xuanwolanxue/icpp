/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: message_broker.h
 *
 * Purpose: message broker base class define
 *
 * Developer:
 *   wen.gu , 2021-06-15
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_MESSAGE_BROKER_H__
#define __ICPP_MESSAGE_BROKER_H__
#include <functional>
#include <vector>
#include <memory>

#include "icpp/com/types.h"
#include "icpp/com/message.h"
#include "icpp/com/serializer.h"
#include "icpp/com/deserializer.h"
/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{

enum class MessageBrokerRole: uint8_t
{
    kUnknown = 0,
    kClient,
    kService,
   // kAPIGateway,
};

/**
 * EndpointId: the endpoint id, which the state of connection changed with current MessageBroker
 * bool:  true: connected, false: disconnected  
 */
using MessageBrokerConnectionStateHandler = std::function<void(EndpointId, bool)>;

struct MessageBrokerInitializeParam
{
    uint8_t protocol_version;
    uint8_t interface_version;
    std::string broker_name;
    MessageBrokerConnectionStateHandler connection_state_handler;
};

/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/

class COM_CLASS MessageBroker
{
public:
    using MessageBrokerPtr = std::shared_ptr<MessageBroker>;
    using IcppErrc = core::IcppErrc;
    using MessagePtr = Message::MessagePtr;
    using MessageHandler = std::function<void(MessagePtr)>;
    using SerializerPtr = Serializer::SerializerPtr;
    using DeserializerPtr = Deserializer::DeserializerPtr;

    using MessageHandlerMap = std::map<MessageId, MessageHandler>;
    using MessageMap = std::map<MessageType, MessageHandlerMap>;
    /**
    * a function handler to send message and receive response message with callback
    * MessagePtr:  send message
    * MessageType: reply type
    * MessageHandler: reply handler 
    */
    using SendAndReplyHandler = std::function<IcppErrc(MessagePtr, MessageType, MessageHandler)>;

    /**
     * a function handler to send message
     * MessagePtr: point to a message which will be send by current handler 
     */
    using SendMessageHandler = std::function<IcppErrc(MessagePtr)>;
public:
    MessageBroker()
    {
        /** todo something */
    }

    virtual ~MessageBroker(){}

public:
    virtual IcppErrc initialize(const UrlList& urls, const MessageBrokerInitializeParam& param) = 0;
    virtual IcppErrc uninitialize() = 0;
    virtual IcppErrc start() = 0;
    virtual IcppErrc stop() = 0;

public: /** for message: create, serialize, deserialize */
    virtual MessagePtr newMessage(MessageType type, MessageId msg_id, PayloadPtr payload_ptr = nullptr) = 0;
    virtual SerializerPtr newSerializer() = 0;
    virtual DeserializerPtr newDeserializer(PayloadPtr payload) = 0;

public:
    /** register a specific message handler with type and id */
    virtual IcppErrc registerMessageHandler(MessageType type, MessageId message_id, MessageHandler handler) = 0;
    virtual IcppErrc unregisterMessageHandler(MessageType type, MessageId message_id) = 0; /**unregister a specific message handler with type and id */   

    /** if register message handler with 'type', 'message id' and 'handler', 
     * then curent registered 'parent' handler will not be called when 'message id' is received.
     **/
    virtual IcppErrc registerMessageHandler(MessageType type, MessageHandler handler) = 0; /** register message handler for all message id with the type */
    virtual IcppErrc unregisterMessageHandler(MessageType type) = 0; /** unregister all message handler with the message type */

    /** register a message handler and send a subscribe message */
    virtual IcppErrc subscribe(MessageType msg_type, MessageType subscribe_type, MessageId message_id, MessageHandler handler) = 0;
    virtual IcppErrc unsubscribe(MessageType msg_type, MessageType unsubscribe_type, MessageId message_id) = 0;
    virtual IcppErrc resubscribe(MessageType msg_type, MessageType subscribe_type) = 0;
public: /** these functions will only be effective after start() is called */    
    virtual IcppErrc sendMessage(MessagePtr msg_ptr) = 0; /** just send message */
    virtual IcppErrc sendAndReply(MessagePtr msg_ptr, MessageType reply_type, MessageHandler  reply_handler) = 0; /** send message, and set a reply handler to process reply message */
public:
    virtual uint8_t protocol_version() const = 0;
    virtual uint8_t interface_version() const = 0;
    virtual const std::string& broker_name() const = 0;
    virtual MessageBrokerRole role() const = 0;
};


} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_MESSAGE_BROKER_H__ */

