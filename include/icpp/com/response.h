/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: response.h
 *
 * Purpose: response type define
 *
 * Developer:
 *   wen.gu , 2021-08-31
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_COM_RESPONSE_H__
#define __ICPP_COM_RESPONSE_H__
#include <functional>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <list>

#include "icpp/com/types.h"
#include "icpp/com/result.h"

/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{

/** Specifies the state of a Response as returned by wait_for() and wait_until().
 *  These definitions are equivalent to the ones from std::future_status. However, no item
 *  equivalent to std::future_status::deferred is available here.
 *  The numerical values of the enum items are implementation-defined
*/
enum class ResponseStatus : uint8_t
{
    kReady, /** the shared state is ready */
    kTimeout, /** the shared state did not become ready before the specified timeout has passed */

};
/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/
namespace  internal_impl
{
template<typename ResponseType, typename ErrorType = core::IcppErrc>
struct InternalState
{
public:
    using ValueType = Result<ResponseType, ErrorType>;
    using ValueTypePtr = std::shared_ptr<ValueType>;
    /**
     * ResponseType* :  a pointor to the Response value
     * ErrorType:  the error code 
     */
    using ResonseHandler = std::function<void(const ValueType&)>;
    using InternalHandlers = std::list<ResonseHandler>;

public:

    bool is_complete_ = false;
    ValueTypePtr val_ptr_ = nullptr;
    std::mutex lock_;
    std::condition_variable cond_;
    
    InternalHandlers handlers_;
};

}

template<typename ResponseType, typename ErrorType>
class ResponseBuilder;


template<typename ResponseType, typename ErrorType = core::IcppErrc>
class Response final
{
public:
    using ValueType = Result<ResponseType, ErrorType>;
    /**
     * const Result<ResponseType, ErrorType>& : the result of response.
     */
    using ResponseHanlder = std::function<void(const ValueType& )>;
    
private:
    //std::future<Result> real_ret_;
    using InternalState = internal_impl::InternalState<ResponseType, ErrorType>; 
    using InternalStatePtr = std::shared_ptr<InternalState>;
    InternalStatePtr internal_state_ = nullptr;
public:
    Response()
    {
        /** todo something */
    }

    Response(Response&& other)
    :internal_state_(std::move(other.internal_state_))
    {
        /** todo something */
    }


    ~Response()
    {
        /** todo something */
    }
public:

    Response& operator= (Response&& other)
    {
        internal_state_ = std::move(other.internal_state_);
        return *this;
    }

public:
    Response(const Response& other) = delete;
    Response& operator= (const Response& other) = delete;
public: /** for async process response */
    Response& addHandler(ResponseHanlder handler)
    {
        InternalState* is = internal_state_.get();

        core::AutoLock al(is->lock_);

        if (is->is_complete_)
        {
            al.unlock();
            handler(*(is->val_ptr_.get())); /** todo, is aways not nullptr?? */
        }
        else
        {
            is->handlers_.push_back(handler);
        }

        return *this;
    }

public:
    ResponseType get()
    {
        return getResult().ValueOrThrow();
    }

    ValueType getResult() noexcept
    {
        if (!valid())
        {
            return ValueType::FromError(core::IcppErrc::NoState);
        }  

        wait_l();
        InternalState* is = internal_state_.get();

        return std::move(*(is->val_ptr_.get()));
    }

public:
    bool valid() const noexcept
    {/** todo is need care about:  only allow get once from response.  now don't care about it */
        return internal_state_ != nullptr;
    }

    bool isReady() const
    {
        return (internal_state_ != nullptr) && (internal_state_->is_complete_ == true);
    }

    void wait() const
    {
        if (!valid())
        {
            throw std::runtime_error("Response is no state");
        }

        wait_l();
    }

    template <typename Rep, typename Period>
    ResponseStatus waitFor(std::chrono::duration< Rep, Period > const& timeoutDuration) const
    {
        if (!valid())
        {
            throw std::runtime_error("Response is no state");
        }

        InternalState* is = internal_state_.get();

        core::AutoLock al(is->lock_);
        std::cv_status status = is->cond_.wait_for(al, timeoutDuration);

        return (status == std::cv_status::no_timeout) ? ResponseStatus::kReady : ResponseStatus::kTimeout;
    }

    template <typename Clock, typename Duration>
    ResponseStatus waitUntil(std::chrono::time_point< Clock, Duration > const& deadline) const
    {
        if (!valid())
        {
            throw std::runtime_error("Response is no state");
        }

        InternalState* is = internal_state_.get();

        core::AutoLock al(is->lock_);
        std::cv_status status = is->cond_.wait_until(al, deadline);

        return (status == std::cv_status::no_timeout) ? ResponseStatus::kReady : ResponseStatus::kTimeout;
    }

private:
    void wait_l() const
    {
        InternalState* is = internal_state_.get();

        core::AutoLock al(is->lock_);
        while (!is->is_complete_)
        {
            is->cond_.wait(al);
        }
    }

private:
    friend class ResponseBuilder<ResponseType, ErrorType>;

    Response(InternalStatePtr internal_state)
    :internal_state_(internal_state)
    {
        /**todo something */
    }

      
};


template<typename ErrorType>
class Response<void, ErrorType> final
{
public:
    using ValueType = Result<void, ErrorType>;
    /**
     * const Result<void, ErrorType>& : the result of response.
     */
    using ResponseHanlder = std::function<void(const ValueType& )>;
    
private:
    //std::future<Result> real_ret_;
    using InternalState = internal_impl::InternalState<void, ErrorType>; 
    using InternalStatePtr = std::shared_ptr<InternalState>;
    InternalStatePtr internal_state_ = nullptr;
public:
    Response()
    {
        /** todo something */
    }

    Response(Response&& other)
    :internal_state_(std::move(other.internal_state_))
    {
        /** todo something */
    }


    ~Response()
    {
        /** todo something */
    }
public:

    Response& operator= (Response&& other)
    {
        internal_state_ = std::move(other.internal_state_);
        return *this;
    }

public:
    Response(const Response& other) = delete;
    Response& operator= (const Response& other) = delete;
public: /** for async process response */
    Response& addHandler(ResponseHanlder handler)
    {
        InternalState* is = internal_state_.get();

        core::AutoLock al(is->lock_);

        if (is->is_complete_)
        {
            al.unlock();
            handler(*(is->val_ptr_.get())); /** todo, is aways not nullptr?? */
        }
        else
        {
            is->handlers_.push_back(handler);
        }

        return *this;
    }

public:
    void get()
    {
        return getResult().ValueOrThrow();
    }

    ValueType getResult() noexcept
    {
        if (!valid())
        {
            return ValueType::FromError(core::IcppErrc::NoState);
        }  

        wait_l();
        InternalState* is = internal_state_.get();

        return std::move(*(is->val_ptr_.get()));
    }

public:
    bool valid() const noexcept
    {/** todo is need care about:  only allow get once from response.  now don't care about it */
        return internal_state_ != nullptr;
    }

    bool isReady() const
    {
        return (internal_state_ != nullptr) && (internal_state_->is_complete_ == true);
    }

    void wait() const
    {
        if (!valid())
        {
            throw std::runtime_error("Response is no state");
        }

        wait_l();
    }

    template <typename Rep, typename Period>
    ResponseStatus waitFor(std::chrono::duration< Rep, Period > const& timeoutDuration) const
    {
        if (!valid())
        {
            throw std::runtime_error("Response is no state");
        }

        InternalState* is = internal_state_.get();

        core::AutoLock al(is->lock_);
        std::cv_status status = is->cond_.wait_for(al, timeoutDuration);

        return (status == std::cv_status::no_timeout) ? ResponseStatus::kReady : ResponseStatus::kTimeout;
    }

    template <typename Clock, typename Duration>
    ResponseStatus waitUntil(std::chrono::time_point< Clock, Duration > const& deadline) const
    {
        if (!valid())
        {
            throw std::runtime_error("Response is no state");
        }

        InternalState* is = internal_state_.get();

        core::AutoLock al(is->lock_);
        std::cv_status status = is->cond_.wait_until(al, deadline);

        return (status == std::cv_status::no_timeout) ? ResponseStatus::kReady : ResponseStatus::kTimeout;
    }

private:
    void wait_l() const
    {
        InternalState* is = internal_state_.get();

        core::AutoLock al(is->lock_);
        while (!is->is_complete_)
        {
            is->cond_.wait(al);
        }
    }

private:
    Response(InternalStatePtr internal_state)
    :internal_state_(internal_state)
    {
        /**todo something */
    }

    friend class ResponseBuilder<void, ErrorType>;  
};

} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_COM_RESPONSE_H__ */

