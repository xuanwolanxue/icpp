/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: service_types.h
 *
 * Purpose: type defines for client/service communication framework
 *
 * Developer:
 *   wen.gu , 2021-08-20
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_COM_SERVICE_TYPES_H__
#define __ICPP_COM_SERVICE_TYPES_H__
#include <functional>
#include <vector>
#include <memory>

#include "icpp/com/types.h"
#include "icpp/com/serializable.h"
#include "icpp/com/deserializable.h"
#include "icpp/com/serializer.h"
#include "icpp/com/deserializer.h"
/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{

enum class ServiceActivityStatus: uint8_t
{
    kOffline = 0, /** service not boot or not online */
    kOnline,      /** service is online  and keep running */
    kSuspend,     /** service is online but not running(mean that not reseponse method call, not broadcast event and so on)*/
    /** todo, is need add  the status of suspend, running  and so on.*/
};

/** service inforamtion define */
struct ServiceInfo: public Seriablizable, public Deserializable
{
    uint8_t interface_version = 0;
    ServiceId service_id = INVALID_SERVICE_ID;
    std::string service_name;
    UrlList urls;    
    /** todo, add other params */
public:
    bool serialize(Serializer *ser_to) const override
    {
        return ser_to ? ser_to->serialize(interface_version, service_id, service_name, urls) : false;
    }

    bool deserialize(Deserializer *deser_from) override
    {
        return deser_from ? deser_from->deserialize(interface_version, service_id, service_name, urls) : false;
    }

public:
    bool operator==(const ServiceInfo& other)
    {
        return (urls == other.urls) && (service_name == other.service_name) && 
               (interface_version == other.interface_version) && (service_id == other.service_id);
    }
};

struct ServiceActivityStatusInfo: public Seriablizable, public Deserializable
{
    ServiceActivityStatus status = ServiceActivityStatus::kOffline;
    ServiceInfo srv_info;

    /** todo, add other params */
public:
    bool serialize(Serializer *ser_to) const override
    {
        return ser_to ? ser_to->serialize(status, srv_info) : false;
    }

    bool deserialize(Deserializer *deser_from) override
    {
        return deser_from ? deser_from->deserialize(status, srv_info) : false;
    }
};

/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/


} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_COM_SERVICE_TYPES_H__ */

