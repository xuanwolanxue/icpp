/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: message_broker_simple.h
 *
 * Purpose: message broker simple implementation
 *
 * Developer:
 *   wen.gu , 2021-08-20
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_MESSAGE_BROKER_SIMPLE_H__
#define __ICPP_MESSAGE_BROKER_SIMPLE_H__
#include <functional>
#include <vector>
#include <atomic>

#include "icpp/com/types.h"
#include "icpp/com/message_broker.h"
#include "icpp/com/endpoint_manager.h"
/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{
struct EndpointInfo
{
    Endpoint* ep;
    EndpointId endpoint_id;
};
/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/

class COM_CLASS MessageBrokerSimple: public MessageBroker
{
public:
    using IcppErrc = core::IcppErrc;
    using EndpointArray = std::vector<Endpoint*>;
    using EndpointMap = std::map<EndpointId, Endpoint*>;
    using ReplyHandlerMap = std::map<SessionId, MessageBroker::MessageHandler>;
    using ReplyMap = std::map<MessageType, ReplyHandlerMap>;    

public:
    MessageBrokerSimple(MessageBrokerRole role);
    virtual ~MessageBrokerSimple();

public:
    IcppErrc initialize(const UrlList& urls, const MessageBrokerInitializeParam& param) override;
    IcppErrc uninitialize() override;
    IcppErrc start() override;
    IcppErrc stop() override;

public: /** for message: create, serialize, deserialize */
    MessagePtr newMessage(MessageType type, MessageId msg_id, PayloadPtr payload_ptr = nullptr) override;
    SerializerPtr newSerializer() override;
    DeserializerPtr newDeserializer(PayloadPtr payload) override;
public:
 
    /** Register a type of message(means a group message with same type) response handler */
    IcppErrc registerMessageHandler(MessageType type, MessageId message_id,  MessageHandler handler) override;
    IcppErrc unregisterMessageHandler(MessageType type, MessageId message_id) override; /**unregister a specific message handler with type and id */

    /** if register message handler with 'type', 'message id' and 'handler', 
     * then curent registered 'parent' handler will not be called when 'message id' is received.
     **/
    IcppErrc registerMessageHandler(MessageType type, MessageHandler handler) override; /** register message handler for all message id with the type */
    IcppErrc unregisterMessageHandler(MessageType type) override; /** unregister all message handler with the message type */


    /** register a message handler and send a subscribe message */
    IcppErrc subscribe(MessageType msg_type, MessageType subscribe_type, MessageId message_id, MessageHandler handler) override;
    IcppErrc unsubscribe(MessageType msg_type, MessageType unsubscribe_type, MessageId message_id) override;
    IcppErrc resubscribe(MessageType msg_type, MessageType subscribe_type) override;    

public: /** these functions will only be effective after start() is called */    
    IcppErrc sendMessage(MessagePtr msg_ptr) override;
    IcppErrc sendAndReply(MessagePtr msg_ptr, MessageType reply_type,  MessageHandler  reply_handler) override;
public:

    uint8_t protocol_version() const override { return protocol_version_;}
    uint8_t interface_version() const override { return interface_version_;}
    const std::string& broker_name() const override{ return broker_name_; }
    MessageBrokerRole role() const override { return msg_broker_role_; }

private:
    /** return registered message info */
    const MessageMap& registered_message_map();
    const MessageHandlerMap& registered_message_handler_map(MessageType type);


     /**for current broker own endpoint */
    IcppErrc addEndpoint(Endpoint* ep, MessageBrokerConnectionStateHandler connection_state_handler);
    void clearEndpoints();

    void responseError(MessagePtr msg_ptr, IcppErrc err);

    void onMessageProcess(MessagePtr msg_ptr);
    bool checkMessageVersion(MessagePtr msg_ptr);

    /** for source endpoint which connect to current broker */
    void addSourceEndpoint(EndpointId endpoint_id, Endpoint* ep);
    void removeSourceEndpoint(EndpointId endpoint_id);
    void removeSourceEndpoint(Endpoint* ep);  /** remove all endpoint id map with ep */
    void clearSourceEndpoint();

    Endpoint* findEndpoint(EndpointId endpoint_id);

    /** for reply process */
    void addResponseHandler(MessagePtr msg_ptr, MessageType reply_type,  MessageHandler repluy_handler);
    void removeResponseHandler(MessageType type, MessageId message_id, SessionId session_id);

    /** true: have reply handler and successfull processed, then do not call  the message handler register by registerMessageHandler, 
     *  false: havn't reply handler or process failed, then call the message hdnaler register by registerMessageHandler
     */
    bool onReply(MessagePtr msg_ptr);

    SessionId getSessionId();
private:
    bool is_initialized_ = false; 
    bool is_started_ = false;  
    uint8_t protocol_version_ = COM_PROTOCOL_VERSION;
    uint8_t interface_version_ = 0;
    MessageBrokerRole msg_broker_role_;
    std::atomic<EndpointId> next_endpoint_id_;
    std::atomic<SessionId> next_session_id_;

    EndpointManager endpoint_manager_;
    std::string broker_name_;
    EndpointArray endpoints_;
    UrlList urls_;
    MessageMap messages_;
    EndpointMap connected_endpoints_; /** other endpoint connect current message broker */
    ReplyMap replies_;
};


} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_MESSAGE_BROKER_SIMPLE_H__ */

