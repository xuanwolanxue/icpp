/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: registration_center_client.h
 *
 * Purpose: the client of registration center
 *
 * Developer:
 *   wen.gu , 2021-08-20
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_REGISTRATION_CENTER_CLIENT_H__
#define __ICPP_REGISTRATION_CENTER_CLIENT_H__
#include <functional>
#include <vector>
#include <memory>

#include "icpp/com/types.h"
#include "icpp/com/service_types.h"
#include "icpp/com/response.h"
#include "icpp/com/message_broker.h"
#include "icpp/com/result.h"
#include "icpp/com/service_finder.h"
/******************************************************************************
 **    MACROS
 ******************************************************************************/

/** for sync get service operation, wait forever until get a service */
#define GET_SERVICE_WAIT_INFINITE (-1)

#define INVALID_LISTENER_ID ((uint32_t)0xffffffff)
/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{

/** the messsage id(method/api id) define for registration center service */
enum class RegistrationCenterMethod: MethodId
{
    kUnknown = 0,

    /** for service side */
    kAllocUrls,       /** method param: std::string service_name, return UrlList */
    kRegisterService, /** method param: std::string ServiceInfo, return void */
    kUnregisterService, /** method param: std::string service_name, return void */

    /**for client side */
    kGetServiceInfo = 0x100, /** method param: std::string service_name, return ServiceInfo */
    kGetServiceNameList,     /** method param: none, return std::vector<std::string> */
    kGetServiceList,         /** method param: none, return std::vector<ServiceInfo> */
    kIsServiceExist, /** method param: std::string service_name, return bool */

    /** for other */
    kReportAlive = 0x200, /** method param: std::string service_name, return: void */
};

/** the message id(event id) define for registration center service */
enum class RegistrationCenterEvent: EventId
{
    kUnknown = 0,
    kServiceActivityStatusChanged, /** event payload: ServiceActivityStatusInfo */
};

/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/




class COM_CLASS RegistrationCenterClient final
{
public:
    using IcppErrc = core::IcppErrc;
    /**
     *  ServiceActivityStatus: the activity status of service
     *  const ServiceInfo&: service information
     */
    using ServiceActivityStatusListener = std::function<void(ServiceActivityStatus, const ServiceInfo&)>;
    /** bool:  true: the registration center is online, false: offline */
    using RegistrationCenterOnlineHandler = std::function<void(bool)>;
    using MessageBrokerPtr = MessageBroker::MessageBrokerPtr;

    using ServiceFinderPtr = ServiceFinder::ServiceFinderPtr;
    using ServiceInfoArray = std::vector<ServiceInfo>;
    using ServiceNameArray = std::vector<std::string>;
protected:
    RegistrationCenterClient();

public:
    ~RegistrationCenterClient();

    RegistrationCenterClient(const RegistrationCenterClient& other) = delete;
    RegistrationCenterClient& operator=(const RegistrationCenterClient& other) = delete;

public: /** for service */
    Response<UrlList> allocUrls(const std::string& service_name); /** allocate adress for service  by registration center */
    Response<void> registerService(const ServiceInfo& param);
    Response<void> unregisterService(const std::string& service_name);

public: /** for client */
    Response<ServiceInfo> getServiceInfo(const std::string& service_anme);
    Response<bool> isServiceExist(const std::string& service_name);
    Response<ServiceNameArray> getServiceNameList();
    Response<ServiceInfoArray> getServiceList();

    /** if success will return a id, which can be as param for remove listener method */
    Response<uint32_t> addListener(const std::string& service_name, ServiceActivityStatusListener listener);

    Response<void> removeListener(const std::string& service_name, uint32_t listener_id);



public: /** for other */
    Response<void> reportAlive(const std::string& service_name);  /** report this registration center client is alive(maybe in service side or client side) */

    Response<void> startAutoReportAlive(const std::string& service_name, uint32_t interval_ms = 1000); /** default report alive status interval is 1 sec */
    Response<void> stopAutoReportAlive(const std::string& service_name);

    /** if success will return a id, which can be as param for remove online handler method */
    Response<uint32_t> addOnlineHandler(RegistrationCenterOnlineHandler handler);
    Response<void>   removeOnlineHandler(uint32_t handler_id); 
public: /** local method for client side */

    static ServiceFinderPtr getServiceFinder(const std::string& service_name);
    static RegistrationCenterClient& getInstance();
private:
    class Impl;
    std::unique_ptr<Impl> impl_;
};



} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_REGISTRATION_CENTER_CLIENT_H__ */

