/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: std_apply.h
 *
 * Purpose: implementation apply template in c++17 to compatible c++11/c++14
 *
 * Developer:
 *   wen.gu , 2021-08-20
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_COM_STD_APPLY_H__
#define __ICPP_COM_STD_APPLY_H__
#include <functional>
#include <tuple>

#include "icpp/com/types.h"

/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{


/** for define index sequence */
#if CPP_STD_VERSION < CPP_STD_VER_14   
/**for the case of < c++14 */
template<size_t...N>
struct IndexSequence {};

//Expand parameter package by inherit
template<size_t N, size_t... Indexes>
struct MakeIndexSequence : MakeIndexSequence<N - 1, N - 1, Indexes...> {};

// terminate parameter package expanding conditional 
template<size_t... Indexes>
struct MakeIndexSequence<0, Indexes...>: public IndexSequence<Indexes...>
{
    //using type = IndexSequence<Indexes...>;
};

template<typename... Args>
using IndexSequenceFor = MakeIndexSequence<sizeof ...(Args)>;

#else /** for the case of >= c++14 */

template<size_t... I>
using IndexSequence = std::index_sequence<I...>;

template<size_t N>
using MakeIndexSequence = std::make_index_sequence<N>;

template<typename... Args>
using IndexSequenceFor = MakeIndexSequence<sizeof ...(Args)>;

#endif
/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/

/** for implementation apply template function */
#if CPP_STD_VERSION < CPP_STD_VER_17   
/** for the case of < c++17 */

template <typename F, typename Tuple, size_t ...I>
inline auto ApplyImpl(F&& f, const Tuple& t, IndexSequence<I...>) -> decltype(f(std::get<I>(t)...))
{
    return f(std::get<I>(t)...);
}


template <class F, class Tuple>
inline auto Apply(F&& f, const Tuple& t) -> decltype(ApplyImpl(std::forward<F>(f), t, MakeIndexSequence<std::tuple_size<Tuple>::value>()))
{
    return ApplyImpl(std::forward<F>(f), t, MakeIndexSequence<std::tuple_size<Tuple>::value>());
}

#else
/** for the case of >= c++17 */
#define Apply(func, t) std::apply(func, t)
#endif

} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_COM_STD_APPLY_H__ */

