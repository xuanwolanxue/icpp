/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: service_finder.h
 *
 * Purpose: implementation a finder to find the service instance for proxy/client side
 *
 * Developer:
 *   wen.gu , 2021-09-12
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_COM_SERVICE_FINDER_H__
#define __ICPP_COM_SERVICE_FINDER_H__
#include <functional>
#include <vector>
#include <memory>

#include "icpp/com/types.h"
#include "icpp/com/message.h"
#include "icpp/com/service_types.h"
#include "icpp/com/result.h"
#include "icpp/com/message_broker.h"

/******************************************************************************
 **    MACROS
 ******************************************************************************/

/** for sync get service operation, wait forever until get a service */
#define FIND_SERVICE_WAIT_INFINITE (-1)
/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{

/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/

class COM_CLASS ServiceFinder
{
public:
    using OnFindServiceHandler = std::function<void(const Result<ServiceInfo>&)>;
    using ServiceFinderPtr = std::shared_ptr<ServiceFinder>;

public:
    virtual ~ServiceFinder() {/** todo something*/ }
public:
    /**
     * try to find a service instance, and return result immediately.
    */
    virtual Result<ServiceInfo> tryFindService() = 0;

    /**
     * start to find a service instance and wait unitil timeout or successfull find a valid service instance
     */
    virtual Result<ServiceInfo> findService(uint32_t wait_ms = FIND_SERVICE_WAIT_INFINITE) = 0;

    /**
     * cancel find service  when 'findService' method called and in waiting state. 
     */
    virtual Result<void> cancelFindService() = 0;

    /**
     * start to find a service with async mode, the result will return by callback  'handler'
     */
    virtual Result<void> startFindServiceAsync(OnFindServiceHandler handler) = 0;

     /**
     * stop to find a service with async mode
     */   
    virtual Result<void> stopFindServiceAsync() = 0;
};



} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_COM_SERVICE_FINDER_H__ */

