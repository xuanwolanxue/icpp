/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: proxy_property.h
 *
 * Purpose: property base class implementation for proxy/client side.
 *
 * Developer:
 *   wen.gu , 2021-09-9
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_COM_PROXY_PROPERTY_H__
#define __ICPP_COM_PROXY_PROPERTY_H__
#include <functional>
#include <vector>
#include <memory>

#include "icpp/com/types.h"
#include "icpp/com/message.h"
#include "icpp/com/proxy_event.h"
#include "icpp/com/proxy_method.h"
/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{


/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/
template<class RequesterPtr, PropertyId property_id, typename PropertyType>
class COM_CLASS ProxyProperty final : public ProxyEventBase<RequesterPtr, MessageType::kPropertyNotify, MessageType::kPropertySubcribe, MessageType::kPropertyUnsubscribe, property_id, PropertyType>
{
public:
    RequestMehotd<RequesterPtr, MessageType::kPropertySet, MessageType::kPropertySetAck, property_id, PropertyType, const PropertyType& > set;
    RequestMehotd<RequesterPtr, MessageType::kPropertyGet, MessageType::kPropertyGetAck, property_id, PropertyType, void> get;
public:
    ProxyProperty(RequesterPtr requester_ptr)
    :ProxyEventBase<RequesterPtr, MessageType::kPropertyNotify, MessageType::kPropertySubcribe, MessageType::kPropertyUnsubscribe, property_id, PropertyType>(requester_ptr),
    set(requester_ptr),
    get(requester_ptr)
    {
        static_assert(!std::is_void<PropertyType>::value, "the property data type can't be 'void'");
        /** todo something */
    }
};



} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_COM_PROXY_PROPERTY_H__ */

