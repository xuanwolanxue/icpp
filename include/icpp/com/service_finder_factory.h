/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: service_finder_factory.h
 *
 * Purpose: implementation a factory for create finder
 *
 * Developer:
 *   wen.gu , 2021-09-30
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_COM_SERVICE_FINDER_FACTORY_H__
#define __ICPP_COM_SERVICE_FINDER_FACTORY_H__
#include <functional>
#include <vector>
#include <memory>

#include "icpp/com/types.h"
#include "icpp/com/message.h"
#include "icpp/com/service_types.h"
#include "icpp/com/result.h"
#include "icpp/com/message_broker.h"
#include "icpp/com/service_finder.h"

/******************************************************************************
 **    MACROS
 ******************************************************************************/

/** for sync get service operation, wait forever until get a service */
#define FIND_SERVICE_WAIT_INFINITE (-1)
/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{

enum class ServiceFinderType: uint8_t
{
    kUnknown = 0,
    kIpc,    /** all service are in local, communication with local IPC, find service with check IPC adress or check file exist */
    kRegistrationCenter /** all service will be register to Registration center, so find service by Registration center client. */
};

/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/

class ServiceFinderFactory final
{
public:
    using ServiceFinderPtr = ServiceFinder::ServiceFinderPtr;    
    using MakeServiceFinder = std::function<ServiceFinderPtr(const std::string& srv_name)>;
protected:
    ServiceFinderFactory();
public:
    ~ServiceFinderFactory();

public:
    bool addFactroy(ServiceFinderType finder_type, MakeServiceFinder on_new_finder);
public:
    static ServiceFinderFactory& getInstance();
    static ServiceFinderPtr create(ServiceFinderType finder_type, const std::string& srv_name);
private:
    class Impl;
    std::unique_ptr<Impl> impl_;
};





} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_COM_SERVICE_FINDER_FACTORY_H__ */

