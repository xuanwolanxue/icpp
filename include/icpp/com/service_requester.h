/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: service_requester.h
 *
 * Purpose: the requester base class define for the service
 *
 * Developer:
 *   wen.gu , 2021-06-15
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_COM_SERVICE_REQUESTER_H__
#define __ICPP_COM_SERVICE_REQUESTER_H__
#include <functional>
#include <map>

#include "icpp/com/types.h"
#include "icpp/com/message.h"
#include "icpp/com/result.h"
#include "icpp/com/serializer.h"
#include "icpp/com/deserializer.h"
#include "icpp/com/service_finder.h"
/******************************************************************************
 **    MACROS
 ******************************************************************************/
/** for sync connect operation, wait forever until connect to service */
#define CONNECT_WAIT_INFINITE (-1)


/** for sync invoke operation, wait forever unit reply is received */
#define INVOKE_WAIT_INFINITE (-1)

/** for sync invoke operation, default wait 5 sec, if reply isn't received then return timeout error. */
#define INVOKE_WAIT_DEFAULT_MS (5 * 1000)

/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{

/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/

class COM_CLASS ServiceRequester
{
public:
    using IcppErrc = core::IcppErrc;
    using MessagePtr = Message::MessagePtr;
    using MessageBrokerPtr = MessageBroker::MessageBrokerPtr;
    
    using SerializerPtr = Serializer::SerializerPtr;
    using DeserializerPtr = Deserializer::DeserializerPtr;
    using MessageHandler = std::function<void(MessagePtr)>;

    using ServiceRequesterPtr = std::shared_ptr<ServiceRequester>;

    /**bool: true: async connect to service successfull, false: failed */
    using ConnectHandler = std::function<void(bool)>;
    using EventHandler = MessageHandler;

    /** define event group with:
     * MessageId:  event id
     * EventHandler: the handler to process subscribe event.
    */
    using EventHandlerMap = std::map<MessageId, EventHandler>;
public:
    ServiceRequester(ServiceFinder::ServiceFinderPtr service_finder, uint8_t interface_version = 0);
    virtual ~ServiceRequester();

public:
    /**
     * start to connect to service and wait unitil timeout or successfull connect to service
     */
    virtual IcppErrc connect(int32_t timeout_ms = CONNECT_WAIT_INFINITE);

    /**
     * try to connect to service, and return result immediately.
    */
    virtual IcppErrc tryConnect();

    /**
     * cancel connect  when Connect method called and in waiting state. 
     */
    virtual void cancelConnect();

    /** disconnect from service*/
    virtual IcppErrc disconnect();

    /**
     * start to connect to service, the result will return by callback  'handler'
     */
    virtual IcppErrc startConnectAsync(ConnectHandler handler);
    virtual IcppErrc stopConnectAsync();
    
public: /** for method */

    /** invoke with sync mode, this API will not return until reply is received */
    virtual Result<MessagePtr> invoke(MessageId method_id, PayloadPtr param_ptr, int32_t  timeout_ms = INVOKE_WAIT_DEFAULT_MS);

    /** invoke with async mode, when the on_replay_handler be called by receive thread, then it will be removed */
    virtual IcppErrc invokeAsync(MessageId method_id, PayloadPtr param_ptr, MessageHandler on_reply_handler);

    /** fire&forget invoke */
    virtual IcppErrc invokeNoReturn(MessageId method_id, PayloadPtr param_ptr);

public: /** for event */
    virtual IcppErrc subscribe(MessageId event_id, EventHandler handler);
    virtual IcppErrc subscribe(const EventHandlerMap& event_group);

    virtual IcppErrc unsubscribe(MessageId event_id);
    virtual IcppErrc unsubscribe(const EventHandlerMap& event_group);

public: /** for property */
    virtual Result<MessagePtr> propertySet(MessageId property_id, PayloadPtr val_ptr, int32_t  timeout_ms = INVOKE_WAIT_DEFAULT_MS);
    virtual Result<MessagePtr> propertyGet(MessageId property_id, int32_t  timeout_ms = INVOKE_WAIT_DEFAULT_MS);
    virtual IcppErrc propertySubscribe(MessageId property_id, EventHandler handler);
    virtual IcppErrc propertyUnsubscribe(MessageId property_id);


public: /** for message: create, serialize, deserialize */
    /** MessageId:  include: re*/
    virtual MessagePtr newMessage(MessageType type, MessageId message_id, PayloadPtr payload_ptr);

    virtual SerializerPtr newSerializer();
    virtual DeserializerPtr newDeserializer(PayloadPtr payload);

    virtual MessageBrokerPtr broker();

protected:
    class Impl;
    std::unique_ptr<Impl> impl_;

};


} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_COM_SERVICE_REQUESTER_H__ */

