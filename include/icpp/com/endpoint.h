/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: endpoint.h
 *
 * Purpose: communication endpoint base class define
 *
 * Developer:
 *   wen.gu , 2021-06-15
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_COM_ENDPOINT_H__
#define __ICPP_COM_ENDPOINT_H__
#include <functional>
#include <string>
#include <vector>
#include <memory>

#include "icpp/core/component.h"
#include "icpp/com/types.h"
#include "icpp/com/message.h"
/******************************************************************************
 **    MACROS
 ******************************************************************************/

#define ENDPOINT_COMP_TYPE "Endpoint"
/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{

enum class EndpointRole: uint8_t
{
    kUnknown = 0,
    kClient,
    kService,
};

struct EndpointCreateParam: public core::ComponentCreateParam
{
    EndpointRole role = EndpointRole::kUnknown;
    /**
     * the url need include communication protocol. the plugin manager 
     * load endpoint component will scan and check the 'protocol' type.
     * e.g. : 
     * In-process communication:    inproc://test
     * Inter-process communication: ipc:///temp/test.ipc 
     * tcp  communication:          tcp://*:5555
     */
    std::string url = "";
    EXTEND_COMP_CREATE_PARAM(EndpointCreateParam, ENDPOINT_COMP_TYPE)

};


/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/
class COM_CLASS Endpoint: public core::Component
{
public:
    using IcppErrc = core::IcppErrc;
    /**
     * int32_t: endpoint id, who send data to current endpoint
     * PayloadPtr:  received payload data(A complete data packet(or message)) 
     */
    using ReceiveHandler = std::function<void(Message::MessagePtr)>;
    
    /**
     * EndpointId: the endpoint id, which the state of connection  has changed with current endpoint
     * bool:  true: connected, false: disconnected  
     */
    using ConnectionStateHandler = std::function<void(EndpointId, bool)>;
public:
    /**
     * name:     the name of endpoint component
     * version:  the version of endpoint component
     * url:      the url of endpoint(the adress of service)
     * role:     the role of current endpoint
     *  
     */
    Endpoint(const std::string& name, const std::string& version)
    :Component(name, version)
    {
        /** todo something */
    }

    virtual ~Endpoint()
    {
        /** todo something */
    };

public:
    /**initialize endpoint. 
     * if role service, then  do  communication protocol bind(e.g. socket bind)
     * if role client, then try to connect to service endpoint
     */
    virtual IcppErrc start() = 0;

    /** uninitialize endpoint
     * if role service, then do unbind
     * if role client,  then disconect from service endpoint
    */
    virtual IcppErrc stop() = 0;

    /** the common api */
    /**
     * endpoint_id:   == INVALID_ENDPOINT_ID:   (default)broadcast to all endpoints which connected with current endpoint, 
     *                != INVALID_ENDPOINT_ID: send to a special endpoint  which connected with current endpoint
     */
    virtual IcppErrc send(Message::MessagePtr msg_ptr) = 0;
    
    /** must set before 'start' is called */
    virtual void setReceiveHandler(ReceiveHandler handler) = 0;
    virtual void setConnectionStateHandler(ConnectionStateHandler handler) = 0;
public:
    virtual EndpointRole role() const = 0;
    //virtual EndpointId endpoint_id() = 0;
    virtual const std::string& url() const  = 0;
};



} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_COM_ENDPOINT_H__ */

