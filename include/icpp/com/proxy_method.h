/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: proxy_method.h
 *
 * Purpose: define method template for proxy/client side.
 *
 * Developer:
 *   wen.gu , 2021-09-07
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_COM_PROXY_METHOD_H__
#define __ICPP_COM_PROXY_METHOD_H__
#include <functional>
#include <vector>
#include <memory>

#include "icpp/com/types.h"
#include "icpp/com/message.h"
#include "icpp/com/response.h"
#include "icpp/com/response_builder.h"
#include "icpp/com/template_utils.h"
/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{


/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/

template<class RequesterPtr>
Response<Message::MessagePtr> MessageMethod(RequesterPtr requester_ptr, Message::MessagePtr param_ptr, MessageType reply_type)
{
    using ResponseBuilderPtr = std::shared_ptr< ResponseBuilder<Message::MessagePtr>>;
    ResponseBuilderPtr res_buidler_ptr = std::make_shared< ResponseBuilder<Message::MessagePtr> >();
    ResponseBuilder<Message::MessagePtr>* res_buidler = res_buidler_ptr.get();

    core::IcppErrc ret = requester_ptr->sendAndReply(param_ptr, reply_type, [res_buidler_ptr](Message::MessagePtr msg_ptr)
    {
            core::IcppErrc rec =  msg_ptr->error_code();
            if (core::IcppErrc::OK == rec)
            {/** haven't error, so process response data */
                res_buidler_ptr->setValue(std::move(msg_ptr));
            }
            else
            {
                res_buidler_ptr->setError(rec);
            }
    });

    if (core::IcppErrc::OK != ret)
    {
        res_buidler->setError(ret);            
    }

    return res_buidler->getResponse();
}

template<class RequesterPtr>
Response<Message::MessagePtr> MessageMethod(RequesterPtr requester_ptr, MessageType request_type, MessageType reply_type, MessageId msg_id, PayloadPtr payload_ptr)
{
    Message::MessagePtr msg_ptr = requester_ptr->newMessage(request_type, msg_id, payload_ptr);
    return MessageMethod(requester_ptr, msg_ptr, reply_type);
}

template<class RequesterPtr>
core::IcppErrc MessageMethodAsync(RequesterPtr requester_ptr, Message::MessagePtr param_ptr, MessageType reply_type, std::function<void(Message::MessagePtr)> handler)
{
    return requester_ptr->sendAndReply(param_ptr, reply_type, handler);
}

template<class RequesterPtr>
core::IcppErrc MessageMethodAsync(RequesterPtr requester_ptr, MessageType request_type, MessageType reply_type, MessageId msg_id, PayloadPtr payload_ptr, std::function<void(Message::MessagePtr)> handler)
{
    Message::MessagePtr msg_ptr = requester_ptr->newMessage(request_type, msg_id, payload_ptr);
    return MessageMethodAsync(requester_ptr, msg_ptr, reply_type, handler);
}


template<class RequesterPtr, MessageType reply_type, typename ReturnType>
class RequestMethodBase
{
public:
    using ResponseBuilderPtr = std::shared_ptr< ResponseBuilder<ReturnType>>;
    using MessagePtr = Message::MessagePtr;
protected:
    RequesterPtr requester_;
    Deserializer::DeserializerPtr deserializer_ptr_;
    Serializer::SerializerPtr serializer_ptr_;
    Message::NewMessageHandler new_message_handler_;
public:
    RequestMethodBase(RequesterPtr requester)
    :requester_(requester),
    deserializer_ptr_(requester_->newDeserializer(nullptr)),
    serializer_ptr_(requester_->newSerializer())
    {
        /** todo something */
        RequesterPtr req_ptr = requester_;
        new_message_handler_ = [req_ptr](MessageType type, MessageId id, PayloadPtr payload_ptr) -> Message::MessagePtr
        {
            return req_ptr->newMessage(type, id, payload_ptr);
        };
    }

    virtual ~RequestMethodBase()
    {
        /** todo something */
    }  

public:
    Response<ReturnType> onMethod(ResponseBuilderPtr res_buidler_ptr,  MessagePtr param_ptr)
    {
        Deserializer::DeserializerPtr deser_ptr = deserializer_ptr_;
        core::IcppErrc ret = requester_->sendAndReply(param_ptr, reply_type, [deser_ptr, res_buidler_ptr](Message::MessagePtr msg_ptr)
        {
            core::IcppErrc rec =  msg_ptr->error_code();
            if (core::IcppErrc::OK == rec)
            {/** haven't error, so process response data */
                ReturnType val;
                rec = DeserializeValue(deser_ptr, msg_ptr, val);

                if (core::IcppErrc::OK == rec)
                {
                    res_buidler_ptr->setValue(std::move(val));
                }
                else
                {                        
                    //todo refine me?? 
                    res_buidler_ptr->setError(rec);
                }
            }
            else
            {
                res_buidler_ptr->setError(rec);
            }
        });

        if (core::IcppErrc::OK != ret)
        {
            res_buidler_ptr->setError(ret);            
        }
        
        return res_buidler_ptr->getResponse();
    }
};

template<class RequesterPtr, MessageType reply_type>
class RequestMethodBase<RequesterPtr, reply_type, void>
{
public:
    using ResponseBuilderPtr = std::shared_ptr< ResponseBuilder<void>>;
    using MessagePtr = Message::MessagePtr;
protected:
    RequesterPtr requester_;
    Serializer::SerializerPtr serializer_ptr_;
    Message::NewMessageHandler new_message_handler_;
public:
    RequestMethodBase(RequesterPtr requester)
    :requester_(requester),
    serializer_ptr_(requester_->newSerializer())
    {
        /** todo something */
        RequesterPtr req_ptr = requester_;
        new_message_handler_ = [req_ptr](MessageType type, MessageId id, PayloadPtr payload_ptr) -> Message::MessagePtr
        {
            return req_ptr->newMessage(type, id, payload_ptr);
        };        
    }

    virtual ~RequestMethodBase()
    {
        /** todo something */
    }  

public:
    Response<void> onMethod(ResponseBuilderPtr res_buidler_ptr,  MessagePtr param_ptr)
    {
        core::IcppErrc ret = requester_->sendAndReply(param_ptr, reply_type, [res_buidler_ptr](Message::MessagePtr msg_ptr)
        {
            core::IcppErrc rec =  msg_ptr->error_code();
            if (core::IcppErrc::OK == rec)
            {/** haven't error, so process response data */
                res_buidler_ptr->setValue();  
            }
            else
            {
                res_buidler_ptr->setError(rec);
            }
        });

        if (core::IcppErrc::OK != ret)
        {
            res_buidler_ptr->setError(ret);            
        }
        
        return res_buidler_ptr->getResponse();
    }
};




template<class RequesterPtr, MessageType msg_type, MessageType reply_type, MessageId msg_id, typename ReturnType, typename ...Args>
class RequestMehotd: public RequestMethodBase<RequesterPtr, reply_type, ReturnType>
{
public:
    using ResponseBuilderPtr = std::shared_ptr< ResponseBuilder<ReturnType>>;
    using MessagePtr = Message::MessagePtr;
public:
    RequestMehotd(RequesterPtr requester)
    :RequestMethodBase<RequesterPtr, reply_type, ReturnType>(requester)
    {
        /** todo something */
    }

    virtual ~RequestMehotd()
    {
        /** todo something */
    }    

public:
    Response<ReturnType> operator()(Args... args)
    {
        ResponseBuilderPtr res_buidler_ptr = std::make_shared< ResponseBuilder<ReturnType> >();
        ResponseBuilder<ReturnType>* res_buidler = res_buidler_ptr.get();
        MessagePtr param = SerializeArguments(this->new_message_handler_, this->serializer_ptr_, msg_type, msg_id, std::forward<Args>(args)...);
        if (param == nullptr)
        {
            res_buidler->setError(core::IcppErrc::BadParameter);
            return res_buidler->getResponse();
        }

        return this->onMethod(res_buidler_ptr, param);
    }
};

template<class RequesterPtr, MessageType msg_type, MessageType reply_type, MessageId msg_id, typename ReturnType>
class RequestMehotd<RequesterPtr, msg_type, reply_type, msg_id, ReturnType, void>: public RequestMethodBase<RequesterPtr, reply_type, ReturnType>
{
public:
    using ResponseBuilderPtr = std::shared_ptr< ResponseBuilder<ReturnType>>;
    using MessagePtr = Message::MessagePtr;
public:
    RequestMehotd(RequesterPtr requester)
    :RequestMethodBase<RequesterPtr, reply_type,ReturnType>(requester)
    {
        /** todo something */
    }

    virtual ~RequestMehotd()
    {
        /** todo something */
    }
public:
    Response<ReturnType> operator()(void)
    {
        ResponseBuilderPtr res_buidler_ptr = std::make_shared< ResponseBuilder<ReturnType> >();
        ResponseBuilder<ReturnType>* res_buidler = res_buidler_ptr.get();
        Message::MessagePtr param = this->new_message_handler_(msg_type, msg_id, nullptr);
        if (param == nullptr)
        {
            res_buidler->setError(core::IcppErrc::BadParameter);
            return res_buidler->getResponse();
        }

        return this->onMethod( res_buidler_ptr, param);
    }
};


template<class RequesterPtr, MessageType msg_type, MessageType reply_type, MessageId msg_id,  typename ...Args>
class RequestMehotd<RequesterPtr, msg_type, reply_type, msg_id, void, Args...>: public RequestMethodBase<RequesterPtr, reply_type, void>
{
public:
    using ResponseBuilderPtr = std::shared_ptr< ResponseBuilder<void>>;
    using MessagePtr = Message::MessagePtr;
public:
    RequestMehotd(RequesterPtr requester)
    :RequestMethodBase<RequesterPtr, reply_type, void>(requester)
    {
        /** todo something */
    }

    virtual ~RequestMehotd()
    {
        /** todo something */
    }
public:
    Response<void> operator()(Args... args)
    {
        ResponseBuilderPtr res_buidler_ptr = std::make_shared< ResponseBuilder<void> >();
        ResponseBuilder<void>* res_buidler = res_buidler_ptr.get();
        Message::MessagePtr param = SerializeArguments(this->new_message_handler_, this->serializer_ptr_,  msg_type, msg_id, std::forward<Args>(args)...);
        if (param == nullptr)
        {
            res_buidler->setError(core::IcppErrc::BadParameter);
            return res_buidler->getResponse();
        }

        return this->onMethod( res_buidler_ptr, param);
    }
};

template<class RequesterPtr, MessageType msg_type, MessageType reply_type, MessageId msg_id>
class RequestMehotd<RequesterPtr, msg_type, reply_type, msg_id, void, void>: public RequestMethodBase<RequesterPtr, reply_type, void>
{
public:
    using ResponseBuilderPtr = std::shared_ptr< ResponseBuilder<void>>;
    using MessagePtr = Message::MessagePtr;
public:
    RequestMehotd(RequesterPtr requester)
    :RequestMethodBase<RequesterPtr, reply_type, void>(requester)
    {
        /** todo something */
    }

    virtual ~RequestMehotd()
    {
        /** todo something */
    }
public:
    Response<void> operator()(void)
    {
        ResponseBuilderPtr res_buidler_ptr = std::make_shared< ResponseBuilder<void> >();
        ResponseBuilder<void>* res_buidler = res_buidler_ptr.get();
        Message::MessagePtr param = this->new_message_handler_(msg_type, msg_id, nullptr);
        if (param == nullptr)
        {
            res_buidler->setError(core::IcppErrc::BadParameter);
            return res_buidler->getResponse();
        }

        return this->onMethod(res_buidler_ptr, param);
    }
};



template<class RequesterPtr, MethodId method_id, typename ReturnType,  typename ...Args>
class COM_CLASS ProxyMethod final: public RequestMehotd<RequesterPtr, MessageType::kRequest, MessageType::kResponse, method_id, ReturnType, Args...>
{
public:
    ProxyMethod(RequesterPtr request_ptr)
    :RequestMehotd<RequesterPtr, MessageType::kRequest, MessageType::kResponse, method_id, ReturnType, Args...>(request_ptr)
    {
        /** todo something */
    }

};


template<class RequesterPtr, MethodId method_id, typename ...Args>
class COM_CLASS ProxyMethodNoReturn final
{
public:
    using MessagePtr = Message::MessagePtr;
private:
    RequesterPtr requester_ = nullptr; /** a shared ptr*/
    Serializer::SerializerPtr serializer_ptr_;
    Message::NewMessageHandler new_message_handler_;
public:
    ProxyMethodNoReturn(RequesterPtr requester)
    :requester_(requester),
    serializer_ptr_(requester_->newSerializer())
    {
        /** todo something */
        RequesterPtr req_ptr = requester_;
        new_message_handler_ = [req_ptr](MessageType type, MessageId id, PayloadPtr payload_ptr) -> Message::MessagePtr
        {
            return req_ptr->newMessage(type, id, payload_ptr);
        };
    }

public:
    Result<void> operator()(Args... args)
    {
        MessagePtr param = SerializeArguments(new_message_handler_, serializer_ptr_, MessageType::kRequestNoReturn, method_id, std::forward<Args>(args)...);

        core::IcppErrc ret = requester_->sendMessage(param);
        if (core::IcppErrc::OK == ret)
        {
            return Result<void>::FromValue();
        }

        return Result<void>::FromError(ret);
    }
};

template<class RequesterPtr, MethodId method_id>
class COM_CLASS ProxyMethodNoReturn<RequesterPtr, method_id, void> final
{
public:
    using MessagePtr = Message::MessagePtr;
private:
    RequesterPtr requester_ = nullptr; /** a shared ptr*/
public:
    ProxyMethodNoReturn(RequesterPtr request_ptr)
    :requester_(request_ptr)
    {
        /** todo something */
    }
public:
    Result<void> operator()(void)
    {
        MessagePtr param = requester_->newMessage(MessageType::kRequestNoReturn, method_id, nullptr);
        core::IcppErrc ret = requester_->sendMessage(param);
        if (core::IcppErrc::OK == ret)
        {
            return Result<void>::FromValue();
        }

        return Result<void>::FromError(ret);
    }
};


} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_COM_PROXY_METHOD_H__ */

