/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: template_utils.h
 *
 * Purpose: template helper function define
 *
 * Developer:
 *   wen.gu , 2021-09-09
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_COM_TEMPLATE_UTILS_H__
#define __ICPP_COM_TEMPLATE_UTILS_H__
#include <functional>
#include <vector>
#include <memory>
#include <tuple>

#include "icpp/com/types.h"
#include "icpp/com/message.h"
#include "icpp/com/serializer.h"
#include "icpp/com/deserializer.h"
/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{

/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/

template<typename ...Args>
Message::MessagePtr SerializeArguments(Message::NewMessageHandler new_message_handler, Serializer::SerializerPtr ser_ptr, MessageType msg_type, MessageId msg_id, Args... args)
{
    Message::MessagePtr msg_ptr = new_message_handler(msg_type, msg_id, nullptr);
    if (msg_ptr)
    {
        ser_ptr->reset();
        if (ser_ptr->serialize(std::forward<Args>(args)...))
        {
            msg_ptr->set_payload(ser_ptr->payload());
        }
        else
        {
            msg_ptr = nullptr;
        }
    }

    return msg_ptr;
}

template<typename ValueType>
core::IcppErrc DeserializeValue(Deserializer::DeserializerPtr deser_ptr, Message::MessagePtr msg_ptr, ValueType& value)
{
    if (msg_ptr == nullptr)
    {
        return core::IcppErrc::BadParameter;
    }

    deser_ptr->setNewPayload(msg_ptr->payload());

    if (deser_ptr->deserialize(value))
    {
        return core::IcppErrc::OK;
    }

    return core::IcppErrc::Undefined;
}

template<typename Tuple, size_t N>
struct TupleParser
{
    static bool Parser(Deserializer::DeserializerPtr deser_ptr, Tuple& t)
    {
        if (TupleParser<Tuple, N - 1>::Parser(deser_ptr, t))
        {
            return deser_ptr->deserialize(std::get<N- 1>(t));
        }

        return false;
    }
};

template<typename Tuple>
struct TupleParser<Tuple, 1>
{
    static bool Parser(Deserializer::DeserializerPtr deser_ptr, Tuple& t)
    {
        return deser_ptr->deserialize(std::get<0>(t));
    }
};



} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_COM_TEMPLATE_UTILS_H__ */

