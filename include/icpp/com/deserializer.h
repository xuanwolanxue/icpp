/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: deserializer.h
 *
 * Purpose: deserilizer implmentation
 *
 * Developer:
 *   wen.gu , 2021-05-10
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_COM_DESERIALIZER_H__
#define __ICPP_COM_DESERIALIZER_H__
#include <vector>
#include <string>
#include <array>
#include <map>
#include <stdint.h>
#include <memory>

#include "icpp/com/types.h"
#include "icpp/com/deserializable.h"
/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{



/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/
class COM_CLASS Deserializer: public std::enable_shared_from_this<Deserializer>
{
public:
    using DeserializerPtr = std::shared_ptr<Deserializer>;
public:
    Deserializer(const uint8_t* data, uint32_t length, uint32_t start_pos = 0);
    Deserializer(PayloadPtr payload, uint32_t start_pos = 0);
    virtual ~Deserializer();

public:
    virtual bool deserialize(bool& value);
    virtual bool deserialize(uint8_t& value);
    virtual bool deserialize(uint16_t& value);
    virtual bool deserialize(uint32_t& value);
    virtual bool deserialize(uint64_t& value);
    virtual bool deserialize(int8_t& value);
    virtual bool deserialize(int16_t& value);
    virtual bool deserialize(int32_t& value);
    virtual bool deserialize(int64_t& value);
    virtual bool deserialize(float& value);
    virtual bool deserialize(double& value);
    virtual bool deserialize(std::string& value);
    virtual bool deserialize(Deserializable& deser);
    
public:

    template<typename T, typename std::enable_if<!(std::is_integral<T>::value ||  std::is_floating_point<T>::value) >::type>
    bool deserialize(std::vector<T>& val)
    {/** treat vector as dynamic array, |val.size()| element values...| */
        uint32_t size;
        if (!deserialize(size))
        {
            return false;
        }

        val.reserve(size);

        for (size_t i; i < size; i++)
        {
            T tmp;
            if (deserialize(tmp))
            {
                val.push_back(std::move(tmp));
            }
            else
            {
                val.clear();
                return false;
            }
        }

        return true;
    }

    template<typename T, typename std::enable_if<(std::is_integral<T>::value ||  std::is_floating_point<T>::value) >::type>
    bool deserialize(std::vector<T>& val)
    {/** treat vector as dynamic array, |val.size()| element values...| */
        uint32_t size;
        if (!deserialize(size))
        {
            return false;
        }

        if (size > 0)
        {
            val.resieze(size);
            return deserialize_l((uint8_t*)val.data(), size * sizeof(T));
        }

        return true;
    }



    template<typename T, std::size_t N, typename std::enable_if<!(std::is_integral<T>::value ||  std::is_floating_point<T>::value) >::type>
    bool deserialize(std::array<T, N>& val)
    {
        uint32_t size;
        if (!deserialize(size))
        {
            return false;
        }

        if (size <= N)
        {
            for (size_t i; i < size; i++)
            {
                if (!deserialize(val[i]))
                {
                    val.clear();
                    return false;
                }
            }
        }

        return true;
    }

    template<typename T, std::size_t N, typename std::enable_if<(std::is_integral<T>::value ||  std::is_floating_point<T>::value) >::type>
    bool deserialize(std::array<T, N>& val)
    {
        uint32_t size;
        if (!deserialize(size))
        {
            return false;
        }

        if (size <= N)
        {
            return deserialize_l((uint8_t*)val.data(), size * sizeof(T));
        }

        return true;
    }

    template<typename Key, typename Value>
    bool deserialize(std::map<Key, Value>& val)
    {
        bool ret = false;
        uint32_t size;
        if (deserialize(size))
        {
            ret = true;
            for (size_t i; i < size; i++)
            {
                Key tmp_key;
                Value tmp_val;
                if (deserialize(tmp_key) && deserialize(tmp_val))
                {                                  
                    val[tmp_key] = std::move(tmp_val);
                }
                else
                {
                    ret = false;
                    val.clear();
                    break;
                }
            }
        }

        return ret;      
    }

    template<typename T, typename std::enable_if<std::is_enum<T>::value >::type >
    bool deserialize(T& val) /** write for enum */
    {
        return deserialize((uint32_t&)(val));
    }

    
    template<typename T, typename ...Args>
    bool deserialize(T& t, Args&... args)
    {
        if (!deserialize(std::forward<T&>(t)))
        {
            return false;
        }

        return deserialize(std::forward<Args&>(args)...);
    }

public:
    /** set a new payload to deserialize, will reset the state of deserialize */
    virtual bool setNewPayload(PayloadPtr payload_ptr, uint32_t start_pos = 0);

protected:
    virtual bool deserialize_l(uint8_t* data, uint32_t size);
    virtual bool deserialize();  /** nothing todo, just for end the bool deserialize(T& t, Args&... args) */
protected:
    PayloadPtr payload_;
    uint32_t pos_ = 0;
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////



} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_COM_DESERIALIZER_H__ */

