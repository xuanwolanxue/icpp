/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: service_event.h
 *
 * Purpose: event base class implementation for service side
 *
 * Developer:
 *   wen.gu , 2021-09-09
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_COM_SERVICE_EVENT_H__
#define __ICPP_COM_SERVICE_EVENT_H__
#include <functional>
#include <vector>
#include <memory>

#include "icpp/com/types.h"
#include "icpp/com/message.h"
#include "icpp/com/template_utils.h"
/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace com
{


/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/

template<class ProviderPtr, MessageType msg_type, EventId event_id, typename SampleType>
class COM_CLASS ServiceEvent final
{
private:
    ProviderPtr provider_ptr_; 
    Serializer::SerializerPtr serializer_ptr_;
public:
    ServiceEvent(ProviderPtr provider_ptr)
    :provider_ptr_(provider_ptr),
    serializer_ptr_(provider_ptr_->newSerializer())
    {
        /** todo something */
    }
public:
    void send(const SampleType& data)
    {
        ProviderPtr pro_ptr = provider_ptr_;
        Message::MessagePtr msg_ptr = SerializeArguments([pro_ptr](MessageType type, MessageId id, PayloadPtr payload_ptr) -> Message::MessagePtr
        {
            return pro_ptr->newMessage(type, id, payload_ptr);
        }, serializer_ptr_, msg_type, event_id, data);

        provider_ptr_->sendMessage(msg_ptr);
    }
};

} /** namespace com */
} /** namespace icpp */

#endif /** !__ICPP_COM_SERVICE_EVENT_H__ */

