/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: vfs.h
 *
 * Purpose: virtual file system API implementation
 *
 * Developer:
 *   wen.gu , 2019-10-16
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_VFS_IMPL_H__
#define __ICPP_VFS_IMPL_H__

#include <memory>

#include "icpp/core/types.h"
#include "icpp/core/errors.h"

namespace icpp
{
namespace core
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/



/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/

class CORE_CLASS VFS final
{
public:
    /** R: read, 
     *  W: write, 
     *  C: create if file isn't exists, 
     *  T: truncate if exists,
     *  A: only write to file end
     */
    enum class OpenMode :uint8_t
    {
        R = 0, /** read only */
        W,     /** write only */

        /** write only, if file not exist, then create it, 
         *  else then truncate file size to 0 
         */
        WCT,
        /** write only, if file not exist, then create it,
         *  and always write data to the end of file(append).
         */
        WCA,
        RW,  /** read/write */
        RWC, /** read/write, if file not exist, then create it */
        RWA, /** read/write, always write data to the end of file(append) */
        RWT, /** read/write, truncate file size to 0 */
        /** read/write, if file not exist, then create it. 
         *  if file exist, then truncate file size to 0 
         */
        RWCT,
        /** read/write, if file not exist, then create it. 
         *  always write data to the end of file(append) 
         */
        RWCA,
    };
public:
    VFS();
    ~VFS();
public:  
    /*
     *@brief open a file
     *@param name [in] the name of file(include path).
     *@param mode [in] open file with input mode
     *@return if sucess return bool(Result) is true, else return Result has error code.
     *
     **/
    IcppErrc fileOpen(const std::string& fileName, OpenMode mode);
    void fileClose();
public:
    /*
     *@brief read data from file
     *@param offset [in] the offset in file
     *@param buf    [io] data buffer
     *@param size   [in] data size
     *@return if sucess return really read size, else return -1.
     *
     *@note if read to file end, will be return '0'
     *
     **/
    int32_t fileRead(int64_t offset, void* buf, uint32_t size);

    /*
     *@brief write data to file 
     *@param offset [in] the offset in file
     *@param buf    [io] data buffer
     *@param size   [in] data size
     *@return if sucess return really write size, else return -1.
     *
     **/
    int32_t fileWrite(int64_t offset, const void* buf, uint32_t size);

    /*
     *@brief sync file data
     *@return if sucess return bool(Result) is true, else return Result has error code.
     *
     **/
    IcppErrc fileSync();

    /*
     *@brief get file size
     *@return if sucess return file size, else return '0'.
     *
     **/
    int64_t fileSize();

private:
    class impl;
    std::unique_ptr<impl> impl_; 
};

} /** namespace core */
} /** namespace icpp */

#endif /** !__ICPP_VFS_IMPL_H__ */

