/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: plugin_manager.h
 *
 * Purpose: plugin manager implementation
 *
 * Developer:
 *   wen.gu , 2019-08-18
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_CORE_PLUGIN_MANAGER_H__
#define __ICPP_CORE_PLUGIN_MANAGER_H__
#include <vector>

#include "icpp/core/types.h"
#include "icpp/core/errors.h"
#include "icpp/core/component.h"
#include "icpp/core/plugin.h"

/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace core
{

/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/

class CORE_CLASS PluginManager final
{
protected:
    PluginManager();
    ~PluginManager();

public:
    static PluginManager& getInstance();
public:
    IcppErrc load(const std::string& name);
    IcppErrc loadList(std::vector<std::string>& pluginList, bool ignoreError = true);

    IcppErrc unloadAll();
    IcppErrc  find(const ComponentCreateParam* param, Component** comp);
    
    /** 
     * freeable: true: can be free by plugin manager, false: can't free by plugin manager
     * 
    */
    IcppErrc registerPlugin(Plugin* plugin, bool freeable = true); 
private:
    class impl;
    std::unique_ptr<impl> impl_;

};

} /** namespace core */
} /** namespace icpp */

#endif /** !__ICPP_CORE_PLUGIN_MANAGER_H__ */

