/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: directory.h
 *
 * Purpose: directory wrapper implementation
 *
 * Developer:
 *   wen.gu , 2019-10-15
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_CORE_DIRECTORY_H__
#define __ICPP_CORE_DIRECTORY_H__

#include <memory>
#include <string>

#include "icpp/core/types.h"

namespace icpp
{
namespace core
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/



/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/

class CORE_CLASS Directory
{
public:
    static std::shared_ptr<Directory> OpenDir(const std::string& dir);
    static bool IsAccessale(const std::string& path);

    static int MkDir(const std::string& dir);
    static int RmDir(const std::string& dir);
public:
    ~Directory();

protected:
    Directory();

public:
    /** isDir: true: directory, false: file */
    bool foreach(void(*dirFunc)(std::string& path, bool isDir, void* opaque), void* opaque);

private:
    class impl;
    std::unique_ptr<impl> impl_;
};

} /** namespace core */
} /** namespace icpp */

#endif /** !__ICPP_CORE_DIRECTORY_H__ */

