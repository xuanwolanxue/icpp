/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: ini_file.h
 *
 * Purpose: ini file parse API
 *
 * Developer:
 *   wen.gu , 2019-07-03
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_CORE_INI_FILE_H__
#define __ICPP_CORE_INI_FILE_H__

#include <vector>
#include <map>

#include "icpp/core/types.h"
#include "icpp/core/errors.h"

namespace icpp
{
namespace core
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/
 /**
 * if ini file havan't any section,then parser will add a default section
 * user cann't use this section name,it is reserve section name.
 */

#define DEFAULT_SECTION_NAME "__default__"

#define LINE_BUF_MAX_LEN   1024 /** the expression line maximum length */

/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/

typedef std::string IniKey;
typedef std::string IniValue;
typedef std::vector<IniValue> IniValueArray;


typedef std::map<IniKey, IniValueArray> IniSection;

typedef std::map<IniKey, IniSection> IniSectionMap;


/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/
class CORE_CLASS IniFile
{
public:
    IniFile();
    virtual ~IniFile();
public:
    IcppErrc load(const std::string& fileName);

    IcppErrc getValue(const IniKey& section, const IniKey& key, IniValue& val);

    IcppErrc getValues(const IniKey& section, const IniKey& key, IniValueArray& vals);

    IcppErrc getSection(const IniKey& key, IniSection& ms);

    bool hasSection(const IniKey& key);
    bool hasKey(const IniKey& section, const IniKey& key);

    void dump2Str(std::string& str);
private:
    struct impl;
    std::unique_ptr<impl> impl_;
};

} /** namespace core */
} /** namespace icpp */

#endif /** !__ICPP_CORE_INI_FILE_H__ */
