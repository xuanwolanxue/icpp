/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: errors.h
 *
 * Purpose: PandaOS base error code define and helper API implementation.
 *
 * Developer:
 *   wen.gu , 2020-05-11
 *
 * TODO:
 *
 ***************************************************************************/

 /******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_CORE_ERRORS_H__
#define __ICPP_CORE_ERRORS_H__


#include "icpp/core/types.h"
#include "icpp/core/json.h"

namespace icpp
{
namespace core
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/

 /******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
 /** error code of icpp */
enum class IcppErrc: uint8_t
{
    OK = 0,
    Fail = 1,
    BadParameter = 2,
    NoInit = 3,
    Eof = 4,
    NotImplemented = 5,
    NotFound = 6,
    Undefined = 7,
    InvaildName = 8,
    InsufficientResources = 9,
    InvalidStatus = 10,
    OutOfRange = 11,
    UnsupportedType = 12,
    Timeout = 13,
    BadAddress = 14,
    ConnectionAborted = 15, /**< connection aborted(ECONNABORTED) */
    ConnectionAlreadyInProgress = 16, /**< connection already in progress(EALREADY) */
    ConnectionRefused = 17, /**< connection refused(ECONNREFUSED) */
    ConnectionReset = 18, /**< connection reset(ECONNRESET) */
    BadMessage = 19, 
    OperationCanceled = 20, /**< operation canceled(ECANCELED) */
    OperationInProgress = 21, /**< operation in progress(EINPROGRESS) */
    OperationNotPermitted = 22, /**< operation not permitted(EPERM) */
    OperationNotSupported = 23, /**< operation not supported(EOPNOTSUPP) */
    OperationWouldBlock = 24, /**< operation would block(EWOULDBLOCK) */
    OutOfMemory = 25,  
    AlreadyExisted = 26, /**< target already existed */
    ResourceIsBusy = 27, /**< target is busing, */
    Mismatch = 28,       /**< the condition mismatch */
    SerializeFailed = 29,  /**< do serialize operation failed */
    DeserializeFailed = 30, /**< do deserialized operation failed */
    NotReady = 31,  /**< the target not ready yet.*/
    ProtocolVersionErr = 32, /**< the protocol version mismatch */
    InterfaceVersionErr = 33, /**< the interface version mismatch */

    NoState = 34, /** attempt to access Response without an associated state */
    BrokenResponse = 35, /** the asynchronous task(ResponseBuilder) abandoned its shared state */
};
 /******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/

CORE_API const char* ErrorStr(IcppErrc errc);

/**
 * json:
 * {
 *    code: xxx,   //Integer
      description: xxx  //string
 * }
 */
CORE_API void Error2Json(IcppErrc err, Value& val);

/**
 * json:
 * {
 *     code: xxx //Integer
 * }
 *
 * here just need error code in json value
 */
CORE_API IcppErrc Json2Error(const Value& val);
} /** namespace core*/
} /** namespace icpp*/

#endif /** !__ICPP_CORE_ERRORS_H__ */
