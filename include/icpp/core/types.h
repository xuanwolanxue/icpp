/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
  * Name: types.h
  *
  * Purpose: base types
  *
  * Developer:
  *   wen.gu , 2019-07-03
  *
  * TODO:
  *
  ***************************************************************************/

  /******************************************************************************
   **    INCLUDES
   ******************************************************************************/
#ifndef __ICPP_CORE_TYPES_H__
#define __ICPP_CORE_TYPES_H__

#include <stdint.h>
#include <mutex>

/******************************************************************************
 **    MACROS
 ******************************************************************************/
#ifdef __cplusplus
#define CORE_EXTERN_C extern "C"
#else
#define CORE_EXTERN_C 
#endif


#if defined(_MSC_VER) /** !!!for visual studio */
    ////////////////////////////////////////////////////////////////////////////
    /** compat the keyword 'inline' */
#if (_MSC_VER > 1200) && !defined (__cplusplus)
#define inline    _inline  /** to compat keyword 'inline' */
#endif

#ifndef DLL_EXPORT_TAG
#define DLL_EXPORT_TAG CORE_DLL_EXPORT
#endif

#if defined (CORE_DLL_EXPORT)
#define CORE_API_EXPORT __declspec(dllexport)
#else
#define CORE_API_EXPORT

#endif
#else /** for other toolchain */
#define CORE_API_EXPORT 
#endif

    /** general api decorate macro */
#define CORE_API CORE_API_EXPORT /**c++ api */

#define CORE_API_C CORE_EXTERN_C CORE_API_EXPORT /** c api */

/** general class decorate macro*/
#define CORE_CLASS CORE_API_EXPORT

/** general global variable decorate macro(for external reference ) */
#define CORE_VAR  extern CORE_API_EXPORT 

/** general const global variable decorate macro(for external reference ) */
#define CORE_CVAR CORE_VAR const 



 // Conditional NORETURN attribute on the throw functions would:
 // a) suppress false positives from static code analysis
 // b) possibly improve optimization opportunities.
#if !defined(CORE_NO_RETURN)
#if defined(_MSC_VER)
#define CORE_NO_RETURN __declspec(noreturn)
#elif defined(__GNUC__)
#define CORE_NO_RETURN __attribute__((__noreturn__))
#else
#define CORE_NO_RETURN
#endif
#endif


/** for c++ std version define */
#if defined(_MSC_VER)  /** visual studio */
#define CPP_STD_VERION _MSVC_LANG
#elif defined(__GNUC__) || defined(__clang__) /**g++/gcc, clang */
#define CPP_STD_VERION __cplusplus
#else
#error "unknown compile toolchian"
#endif

#define CPP_STD_VER_11 201103L
#define CPP_STD_VER_14 201402L
#define CPP_STD_VER_17 201703L




/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/
namespace icpp
{
namespace core 
{

using AutoLock = std::unique_lock<std::mutex>;

/** for const string */
using ConstString = const std::string;

/** type for data byte order */
enum class DataByteOrder : uint8_t
{
    BigEndian = 0,
    LittleEndian = 1
};
/******************************************************************************
**    CLASSES/FUNCTIONS DEFINITIONS
******************************************************************************/

} /** namespace core */
} /** namespace icpp */


#endif /** !__ICPP_CORE_TYPES_H__ */
