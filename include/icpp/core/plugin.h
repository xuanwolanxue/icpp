/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
  * Name: plugin.h
  *
  * Purpose: plugin implementation
  *
  * Developer:
  *   wen.gu , 2019-08-18
  *
  * TODO:
  *
  ***************************************************************************/

  /******************************************************************************
   **    INCLUDES
   ******************************************************************************/
#ifndef __ICPP_CORE_PLUGIN_H__
#define __ICPP_CORE_PLUGIN_H__

#include <string>

#include "icpp/core/types.h"
#include "icpp/core/errors.h"

/******************************************************************************
**    MACROS
******************************************************************************/

/** plugin header version
 * note: plugin manager will check this version to jduagement current
 *       plugin is compatible or not?
 */
#define PLUGIN_HEADER_VER "00.01.00"

 /** the name of plugin entry, used by plugin manager */
#define PLUGIN_ENTRY_NAME "_PluginBaseEntry"
#define PLUGIN_VER_FUNC_NAME "baseOnPluginHeaderVersion"

/** the entry of plugin, used by plugin implementation
 ** note: this macro can't use in namespace xxx, this is c style function **
 */
#define PLUGIN_ENTRY(pluginClass, name, version, author) \
    icpp::core::Plugin* _PluginBaseEntry(){ \
        icpp::core::Plugin* mp = new pluginClass(name, version, author); \
        return mp; \
    } \
    const char* baseOnPluginHeaderVersion(){ \
        return PLUGIN_HEADER_VER; \
    }

/******************************************************************************
**    TYPE DEFINITIONS
******************************************************************************/
namespace icpp
{
namespace core
{

/******************************************************************************
**    CLASSES/FUNCTIONS DEFINITIONS
******************************************************************************/


struct ComponentCreateParam;
class Component;

class CORE_CLASS Plugin
{
public:
    Plugin(const std::string& name, const std::string& version,  const std::string& author)
        :name_(name),
        version_(version),
        author_(author)
    {
        /** todo somethings */
    }

    virtual ~Plugin() { /** todo something */ }
public:
    virtual const std::string& name() const { return name_; }
    virtual const std::string& version() const { return version_; }
    virtual const std::string& author() const { return author_; }

    /** create a component instance(real plugin must implement this API)
     *@parameter

     * [in] param the parameter to request the component
     * [io] comp  the pointer point to component instance,if failed, then nullptr
     *
     *@return
     *   success: IcppErrc::OK, failed: error code.
     */
    virtual IcppErrc createComponent(const ComponentCreateParam* param, Component** comp) = 0;
protected:
    std::string name_ = "";
    std::string version_ = "";
    std::string author_ = "";
};

} /** namespace core */
} /** namespace icpp */

/** the type of plugin entry function, used by plugin manager */
typedef icpp::core::Plugin* (*pluginEntryFunc_t)();
typedef const char* (*pluginHdrVerFunc_t)();

/** the plugin entry define */
CORE_API_C icpp::core::Plugin* _PluginBaseEntry();

/** return the version of media plugin header */
CORE_API_C const char* baseOnPluginHeaderVersion();

#endif /** !__ICPP_CORE_PLUGIN_H__ */

