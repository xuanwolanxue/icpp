/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: component.h
 *
 * Purpose: compoment base api define
 *
 * Developer:
 *   wen.gu , 2019-08-18
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_CORE_COMPONENT_H__
#define __ICPP_CORE_COMPONENT_H__

#include <string>

#include "icpp/core/types.h"


/******************************************************************************
 **    MACROS
 ******************************************************************************/
#define EXTEND_COMP_CREATE_PARAM(self, compType) \
        public: self():icpp::core::ComponentCreateParam(compType){/** todo something */}

/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/

namespace icpp
{
namespace core
{

/** the base structure of the parameter of create a component insntance */
struct ComponentCreateParam
{
    std::string type = ""; /** component type */

public:
    ComponentCreateParam(const std::string& type_str)
        :type(type_str)
    {
        /** todo something */
    }
};

/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/

 /** the base class of component */
class CORE_CLASS Component
{
public:
    Component(const std::string& name, const std::string& version)
        :comp_name_(name),
        comp_ver_(version)
    {
        /** todo something */
    }

    virtual ~Component() { /** todo something */ }

public:
    virtual const std::string name() const { return comp_name_; }
    virtual const std::string version() const { return comp_ver_; }

protected:
    std::string comp_name_ = "";
    std::string comp_ver_ = "";

};

} /** namespace core */
} /** namespace icpp */

#endif /** !__ICPP_CORE_COMPONENT_H__ */

