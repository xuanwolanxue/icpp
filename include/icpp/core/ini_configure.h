/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

 /***************************************************************************
 * Name: ini_configure.h
 *
 * Purpose: ini configure manager class
 *
 * Developer:
 *   wen.gu , 2019-07-04
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __ICPP_CORE_INI_CONFIGURE_H__
#define __ICPP_CORE_INI_CONFIGURE_H__
#include <string>
#include <vector>
#include <memory>
#include "icpp/core/types.h"

namespace icpp
{
namespace core
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/



/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/

class CORE_CLASS IniConfigure
{
public:
    IniConfigure();
    ~IniConfigure();
public:
    bool load(ConstString& fileName);
    
    bool getValue(ConstString& group, ConstString& key, std::string& val);

    bool getValue(ConstString& group, ConstString& key, int32_t& val);                 
                           
    bool getValues(ConstString& group, ConstString& key, std::vector<std::string>& vals);

private:
    class impl;
    std::unique_ptr<impl> impl_;
};

////////////////////////////////////////////////////////////////////////////////
} /** namespace core */
} /** namespace icpp */

#endif /** !__ICPP_CORE_INI_CONFIGURE_H__ */
