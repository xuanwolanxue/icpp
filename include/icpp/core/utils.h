/*
 * MIT License
 *
 * Copyright (c) 2020 wen.gu <454727014@qq.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/***************************************************************************
 * Name: utils.h
 *
 * Purpose: tool functions
 *
 * Developer:
 *   wen.gu , 2019-07-04
 *
 * TODO:
 *
 ***************************************************************************/

/******************************************************************************
 **    INCLUDES
 ******************************************************************************/
#ifndef __CORE_UTILS_H__
#define __CORE_UTILS_H__

#include <string>

#include "icpp/core/types.h"

namespace icpp
{
namespace core
{
/******************************************************************************
 **    MACROS
 ******************************************************************************/


/******************************************************************************
 **    TYPE DEFINITIONS
 ******************************************************************************/

/******************************************************************************
 **    CLASSES/FUNCTIONS DEFINITIONS
 ******************************************************************************/

//sleep a while unit in milli-second
CORE_API void SleepMs(uint32_t ms);

//sleep a while unit in second
CORE_API void SleepSec(uint32_t sec);

//get current system time unit in milli-second.
CORE_API int64_t NowSysMs();

//get current steady time unit in milli-second.
CORE_API int64_t NowSteadyMs();

CORE_API DataByteOrder GetPlatformByteOrder();


// wstring => string
CORE_API std::string WString2String(const std::wstring& ws);


// string => wstring
CORE_API std::wstring String2WString(const std::string& s);

CORE_API bool StringStartWith(const std::string& str, const std::string& head);

} /** namespace core */
} /** namespace icpp */

#endif /** !__CORE_UTILS_H__ */

